import bpy
import numpy as np 
from mathutils import Matrix
import itertools
from bpy.app.handlers import persistent

def remap_keys(struct):
    # Récupérer toutes les clés du dictionnaire
    keys = list(struct.keys())

    # Trouver la valeur minimale et maximale des clés
    min_key = min(keys)
    max_key = max(keys)

    # Calculer la plage (range) des clés
    key_range = max_key - min_key

    # Remapper les clés sur l'intervalle [0, 1]
    remapped_struct = {((key - min_key) / key_range): value for key, value in struct.items()}

    return remapped_struct

def get_key_for_object(remapped_struct, obj_name):
    # Parcourir le dictionnaire remappé
    for key, obj_list in remapped_struct.items():
        # Vérifier si l'objet donné est dans la liste des objets
        if obj_name in obj_list:
            return key  # Retourner la clé correspondante

    # Si l'objet n'est pas trouvé, retourner None
    return None

def get_points(gp_object) :
    exceptions = ['GPLYR_HELPERS', 'GPLYR_RGH']
    points = []
    mat = Matrix(gp_object.matrix_world)
    for layer in [l for l in gp_object.data.layers if l.info not in exceptions ]:
        if layer.active_frame :
            for stroke in layer.active_frame.strokes :
                points += [mat @ p.co for p in stroke.points]
    return np.array(points)


def get_object_bounds(gp_objects) :
    bounds = {}
    for gp in gp_objects :
        points = get_points(gp)        
        if len(points) > 0 :
            min = np.min(points, axis=0)
            max = np.max(points, axis=0)

            bounds[gp.name] = (min, max)
    return bounds

def hitbox_collision(hitbox1, hitbox2) :
    min1 = hitbox1[0][[0,2]]
    max1 = hitbox1[1][[0,2]]
    min2 = hitbox2[0][[0,2]]
    max2 = hitbox2[1][[0,2]]

    hit_x = any([
        min1[0] >= min2[0] and min1[0] <= max2[0],
        max1[0] >= min2[0] and max1[0] <= max2[0],
        min2[0] >= min1[0] and min2[0] <= max1[0],
        max2[0] >= min1[0] and max2[0] <= max1[0],
    ])

    hit_y = any([
        min1[1] >= min2[1] and min1[1] <= max2[1],
        max1[1] >= min2[1] and max1[1] <= max2[1],
        min2[1] >= min1[1] and min2[1] <= max1[1],
        max2[1] >= min1[1] and max2[1] <= max1[1],
    ])

    return hit_x and hit_y

def get_display_structure(gp_objects, precision) :
        struct = {}
        #bpy.context.view_layer.update()
        for o in gp_objects :
            depth = round(o.matrix_world.to_translation()[1], precision)
            
            if depth in struct.keys() :
                struct[depth].append(o.name)
            else :
                struct[depth] = [o.name]        
        return struct
def is_ok(o):
    if o in list(bpy.context.view_layer.objects):
        return o.type == 'GPENCIL' and  'HLP' not in o.name  and not (o.hide_get() or o.hide_viewport)
    return False

def yfight_check(collection) :

        struct = get_display_structure([o for o in collection.all_objects if is_ok(o)], bpy.context.scene.yfight_precision)
        to_alert = []

        # struct est un dict() qui regroupe les représentations de tous les objects indexés dans l'outil de zpush, organisés par profondeur : 
        # struct = {0.0 : [OBJ1, OBJ2, ...], 5.4 : [OBJ3], ...}

        # il itère par couches de profondeur, et il va checker, dans chaque couche, si deux objects ont des hitbox qui s'intersectent
        # c'est pas 100% précis, tant que les hitbox se touchent c'est détecté comme problématique, meme si les deux grease pencil se touchent pas forcément

        for depth in struct.keys() : 
            if len(struct[depth]) < 2 :
                continue

            gp_objects = [bpy.data.objects[o_name] for o_name in struct[depth]]

            # bounds = {"GP.001" : (min, max), "GP.002" : (min, max)}
            # min : point 3D (x,y,z) avec x = min(x) pour tous les points du gp, y = min(y), z = min(z), et idem pour max
            # "GP.001" nom du Grease Pencil

            bounds = get_object_bounds(gp_objects)
            gp_objects = [gp for gp in gp_objects if gp.name in bounds.keys()]

            for gp1, gp2 in itertools.combinations(gp_objects, 2) :
                if hitbox_collision(bounds[gp1.name], bounds[gp2.name]) :
                    to_alert += [gp1, gp2]
        
        return struct, to_alert

@persistent        
def yfight_handler(scene, self):
    #print('iii')
    if scene.yfight_on:
        struct, y_fights = yfight_check(bpy.context.collection)
        
        remapped_struct = remap_keys(struct)
        #print(remapped_struct)
        for o in bpy.context.collection.all_objects : 
            if is_ok(o):
                tint = None
                if 'YFIGHT_TINT' in o.grease_pencil_modifiers.keys() :
                    tint = o.grease_pencil_modifiers['YFIGHT_TINT']
                if tint :
                    if o in y_fights :                    
                        tint.color = (1,0,0)
                        tint.factor = 0.3
                        
                        if bpy.context.object in y_fights :
                            if o.name in remapped_struct[get_key_for_object(remapped_struct, bpy.context.object.name)] :
                                tint.color = (1,1,0)
                                tint.factor = 1
                            
                    else :
                        i = get_key_for_object(remapped_struct, o.name)
                        if i is not None :
                            tint.color = (0, 2*i, 2- 2*i)
                            tint.factor = 0.3
          
    #print('ooo')

class RT_OT_Y_FIGHT(bpy.types.Operator):
    ''' display visual support for Y fight checking between grease pencil objects of active collection'''
    bl_idname = "rigtools.y_fight"
    bl_label = " show Y fights"
    bl_options = {'UNDO', 'REGISTER'}
    precision : bpy.props.IntProperty(default = 3, min = 1)

    def turn_on(self, context) :
        #struct, y_fights = yfight_check(context.collection)
        #print(y_fights)
        bpy.context.scene.yfight_precision = self.precision
        for o in context.collection.all_objects : 
            if is_ok(o):
                tint = o.grease_pencil_modifiers.new('YFIGHT_TINT', 'GP_TINT')
                #if o in y_fights :
                    #tint.color = (1,0,0)
                opac = o.grease_pencil_modifiers.new('YFIGHT_OPAC', 'GP_OPACITY')
                opac.factor = 0.8
        context.scene.yfight_on = True
        #yfight_handler(self, context)
        
        return
    
    def turn_off(self, context) :
        context.scene.yfight_on = False
        for ob in context.scene.objects :
            if ob.type == 'GPENCIL' :
                to_del = []
                for mod in ob.grease_pencil_modifiers :
                    if mod.name.startswith('YFIGHT_TINT'):
                        to_del.append(mod)
                    if mod.name.startswith('YFIGHT_OPAC'):
                        to_del.append(mod)
                for mod in reversed(to_del) :        
                    ob.grease_pencil_modifiers.remove(mod)
        return 
    
    def execute(self, context) :
        if context.scene.yfight_on == False :
            self.turn_on(context)
        else :
            self.turn_off(context)
        return {'FINISHED'}    
    
