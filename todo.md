# ROADMAP
GP_DRIVERS flip

v0.1.2:
    CLONE MEMBER:
        DONE add unexplicit lateral property flip by list of name (e.g 'FLIP_DRAWINGS')

v0.1.1:
    FLIP ANIMATION:
        SOLVED Avoid GP creation on _R object

v0.1.0 
    main merge

v0.0.12:
    FLIP ANIMATION:
     - DONE restore gp point flip
     - DONE add 'HLP' has gp flip skip 
     
v0.0.11:
CLONE MEMBERS:
 - DONE Transpose GP_Cutout general 'PROPERTIES' and 'NON_NATIVE_DATA' conform case to flip case
 - DONE make it work both with and without Ewilan rig Addon activated
 - SOLVED some pointers are accessed by several properties and Datas, making them flip several times. Add lat_index control
 - DONE Add level offset on verbose print in console for clarity

v0.0.10
 - SOLVED flip articulation driver not flipped

v0.0.9:
  - DONE Try/catch IDPropertyArray . 
  - TOCHECK:Try to handle IDPropertyArray.

v 0.0.8:
 CLONE:
  - SOLVED driver issue: lateralized drivers variable pointers not flipped



v 0.0.7:
 FLIP:
  - SOLVED? Nonetype frame exception. Impossible to reproduce. Added catch with warning message if it popup again  
 CLONE:
  - DONE look for lateralised objects into properties that are of type Pointer, List of Pointer or IDPropertyGroup and check if it is pointong to the right side
  - DONE make sure armature bones contraints points to the right targets


v0.0.6:
    DONE Better method for GP stroke flip to avoid "card" effect on auto interpolation.
    DONE merged GP_Cutout common anim_utils.py and general_utils.py to handle new ik rig structure
    SOLVED bug with constraints conformation without targets dir

v0.0.5
    Update common/GP_UTILS to bring pseudo_stroke and pseud points

V 0.0.4
    FLIP ANIMATION
        REMOVED old flip animation operator
        DONE add a flip exeption to GP layers with 'HELPERS' in names
        DONE add fliped keyframe conformation to source keyframe type 
        SOLVED issues with getting the keyframe based on objects visible in dopesheet
        SOLVED some abject having a 'FLIP' porperty are behing flip anyway



patch 03
    SOLVED on salim earth v60, chest flip animation result in x3 copy: This was caused by some keyframe selected on the foot an legs, extending frame range to 28. get_dopesheet_selected modified to use selected only keyframes when there isn't a 'dopesheet.filter_collection'

Patch 02:
    DONE avoid CTRL flip
    SOLVED need to manually flip root object x location FTM.
    SOLVED nose and mouth FLIP property set doesnt work. For some reason, getting mouth flip property doesn't work while nose worked. I had to directly search for FC with FLIP datapath to forbid flip. If GP flip where to be called before FCURVE flip, this would lead to those GP being flipped.
    SOLVED infinite loop on recall flip on flipped frame
    IMPROVED performance


patch 01:
    Test Salim V57
    SOLVED on keyframe mirroring, blocking error if object not found
    SOLVED creating 001.blend on some object because they HAVE BEEN. solved by renaming twice.
    SOLVED ACCESS VIOLATION occuring on colormatch function. Mute Function as it seem useless here but there is an issue with it
    NO BUG invert Z lead to reversing layer order. Was probably linked to the test file
    NO BUG artefact gp keys on frame without animation. Was probably linked to the test file

V0.0.3
    DONE update common to cutout v0.1.5
    CLONE MEMBER
        DONE Add an operator to automatically mirror the keyframe based on a framerange
        BUG invert Z lead to reversing layer order
    FLIP_ANIMATION OPS
        DONE use confo animation  to avoid recreating solved bugs
        DONE simplify and factorize into a global flip fonction in ANIM UTILS
        BUG artefact gp keys on frame without animation
        BUG need to manually flip root object x location FTM.
    ANIM_UTILS
        DONE get dopesheet frame selection range Done
        DONE get dopesheet object selection to avoid useless confo
        FLIP_ANIMATION:
            DONE create
            DONE add an option to ignore not lateralized object.
        ANIMATION_CONFO:
            DONE add a 'selected only curves' option

    SOLVED if a frame of an hidden object is selected, object is flipped
    WARNING if a collection is exclude from view layer, it is not hidden in the tree!

V0.0.2
    CLONEFLIP_MEMBER
        - DONE Use RegEx pattern for suffix to make them easy to change
        - DONE simplify and factorise 
        - DONE use existing common library function:
            - DONE simple common function 
            - DONE use conform_collection:
                - DONE create missing collections and link them to source collection
                - TOTEST what if source collection is lateralized?
            - DONE use conform_object()   
            - DONE link new objects to the right collections.  
        - DONE avoid existing object duplication on multiple call or partial rebuild
        - DONE Debug
    PASTE_TO_SYM
        - DONE Create 'flip animation' operator and button in RigToolBox
    BUG:
        SOLVED: bpy.ops.object.select_all(action='DESELECT') context error
        SOLVED c.target.name with target Nonetype
        SOLVED root object definition is off. This is beacuse root object arent root object, but rooted object.

# FEATURE:
    PASTE_TO_SYM
        - RegEx suffix system
        - Factorise and use existing common library function
        - Debug
    - add suffix definition in addon properties
    - automatic GPframe mirroring
    - link new objects to the right collections. (for the duplicate
                                                  if source object collection is lateralised, put in an equaly leteralised
                                                  else pu in the same
                                                  for flipper: put in the same as C source)
    - use new get_children function to be able to get children of constraints used for IK rig?

 
# BUG:
    -SOLVED if flipper object already exist, flip doesnt occur (parented after flip = no flip)

    -SOLVED ear doesnt work (no head c flip created)

    -SOLVED overall, collection selection dependence is painfull
    
    -SOLVED at some point, context mode get locked (only Object mode available). I had this bug once before, linked to setting mode by ops

    -SOLVED create flipped object outside of collections

    pip install --target=c:path/to/loca/package/ shapely==2.0.2
    
    pip install --target=C:/Users/gerar/AppData/Roaming/Blender Foundation/Blender/3.5/scripts/modules shapely==2.0.2
    C:\Users\gerar\AppData\Roaming\Blender Foundation\Blender\3.5\scripts\modules

    import subprocess
import sys
import os

import subprocess
import sys
import os

def install_pip_dep(module_name):
        python_path = sys.executable
        subp = subprocess.run([python_path, "-m", "ensurepip"])
        print(subp.returncode) 
        if subp.returncode != 0:
            return False
        import importlib.util
        spam_spec = importlib.util.find_spec(module_name)
        found = spam_spec is not None
#        if found:
#            print('module found')
#            return False

        subp = subprocess.run([python_path, "-m", "pip", "install",
                               "--target=" + os.path.join(r'C:\Users\gerar\AppData\Roaming\Blender Foundation\Blender\3.5\scripts', "modules"),
                               module_name, "--upgrade"])
        if subp.returncode != 0:
            return False
        return True

install_pip_dep('shapely')

