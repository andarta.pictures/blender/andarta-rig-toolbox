import re
import bpy
from bpy_extras import view3d_utils
import numpy as np
from bpy.types import Operator
from bpy.props import EnumProperty, BoolProperty, IntProperty
from .common.regex_utils import regex_get_lateral_index
from .common.general_utils import conform_collections, conform_object, regex_flip_name
from .common.gp_utils import get_now_gpframe, move_gp_frame
from .common.anim_utils import deselect_all_keyframes, flip_animation, get_dopesheet_selected, get_now_kf, snap_parented_object
from .common import geometry_3D as geo3D
from mathutils import Matrix, Vector

verbose = False
''' empty ops template :

class RT_OT_TEST(Operator):
    """Tooltip"""
    bl_idname = "rigtools.test_operator"
    bl_label = "test Object Operator"

    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        
        return {'FINISHED'}
'''
def compute_delta(mat, vector):
    v = Vector(vector)
    v.rotate(mat.to_euler())
    v *= mat.to_scale()
    return v
def get_parent(ob) :
    if ob.parent :
        return ob.parent
    for c in ob.constraints :
        if c.type == 'CHILD_OF' and c.influence == 1 and c.target is not None:
            parent = c.target
            return parent
    return None

def get_childs(ob):
    childs = []
    flipped_childs = []
    for o in ob.children :
        childs.append(o, False)
    for o in  bpy.context.scene.objects :
        for c in o.constraints :
            if c.type == 'CHILD_OF' and c.influence == 1 and c.target == ob :
                if o.name.endswith('_FLIP') :
                    for grandchild, dummy in get_childs(o):
                        childs.append((grandchild, True))
                else :
                    childs.append((o, False))
    return childs

def is_flipped(obj) :
    obj_scale = obj.matrix_world.to_scale()
    if obj_scale[0] * obj_scale[1] * obj_scale[2] < 0 :
        if 'FLIP' in obj.keys() and obj['FLIP'] == 0:   
            return False

        else :
            return True
    return False

class RIGTOOLS_OT_place_gp_origin(bpy.types.Operator):
    """Place origin point of Grease Pencil object. 
    Click to replace on current frame only with the mouse,
    ALT click to replace on current frame using 3d cursor location
    CTRL click to replace over the whole timeline"""

    bl_idname = "rigtools.gp_place_gp_origin"
    bl_label = "Place Grease Pencil origin..."
    bl_options = {'REGISTER', 'UNDO'}
    crtl : bpy.props.BoolProperty(default = False, options = {'HIDDEN'})
    alt : bpy.props.BoolProperty(default = False, options = {'HIDDEN'})
    def modal(self, context, event) :
        
        if event.type == "LEFTMOUSE" and event.value == "PRESS" : 
            self.mouse_position = (event.mouse_region_x, event.mouse_region_y)
            self.execute(context)
            context.window.cursor_set("DEFAULT")
            return{'FINISHED'}

        if (event.type == "ESC" and event.value == "PRESS") or (event.type == "RIGHTMOUSE" and event.value == "PRESS") : 
            return {'CANCELLED'}
        return {'RUNNING_MODAL'}

    def invoke(self, context, event):
        obj = context.object
        self.ctrl = event.ctrl
        self.alt = event.alt
        if not obj or obj.type != 'GPENCIL':
            self.report({'ERROR'}, "/!\\ Place Grease Pencil origin : no grease pencil selected")
            return {'CANCELLED'}
        self.obj = obj
        self.r3d, self.region = geo3D.get_region_settings()
        self.init_frame = context.scene.frame_current

        if self.alt : 
            self.mouse_position = view3d_utils.location_3d_to_region_2d(self.region, self.r3d, context.scene.cursor.location)
            self.execute(context)
            return{'FINISHED'}

        context.window_manager.modal_handler_add(self)
        context.window.cursor_set("CROSSHAIR")
        return {'RUNNING_MODAL'}
    
    def execute(self, context):
        context.view_layer.update()
        world_pos = geo3D.region_to_location(self.mouse_position, self.obj.matrix_world.to_translation(), self.region, self.r3d)
        world_delta = world_pos - self.obj.matrix_world.to_translation()

        inv_object = Matrix(self.obj.matrix_world).inverted()
        local_delta = compute_delta(inv_object, world_delta) #translation vector in local space


        if self.ctrl : #over all animation
            #offset gp frames
            for layer in self.obj.data.layers :
                for frame in layer.frames :
                    context.scene.frame_set(frame.frame_number)        
                    context.view_layer.update()  
                    flipped_obj = is_flipped(self.obj)
                    for stroke in frame.strokes :
                        for point in stroke.points :
                            if flipped_obj or ( not flipped_obj and 'FLIP' in self.obj.keys() and self.obj['FLIP'] == 0):
                                point.co += local_delta
                            else: 
                                point.co -= local_delta
            #offset childs 
            for child, flipped in get_childs(self.obj): 
                #print('CHILD', child.name, flipped)          
                if child.animation_data and child.animation_data.action :
                    child_timings = []
                    for fc in child.animation_data.action.fcurves :
                        if fc.data_path == 'location' :
                            for kf in fc.keyframe_points :
                                if int(kf.co.x) not in child_timings :
                                    child_timings.append(int(kf.co.x))
                    for t in reversed(child_timings) :
                        context.scene.frame_set(t)        
                        context.view_layer.update()  
                        flipped_obj = is_flipped(self.obj)
                        if flipped_obj :
                            child_delta = local_delta * -1
                        else : 
                            child_delta = local_delta
                        if not flipped :
                            child.location -= child_delta
                        else : 
                            child.location -= child_delta * Vector((-1,1,1))
                        child.keyframe_insert(data_path = 'location', index = -1) 
                else :
                    flipped_obj = is_flipped(self.obj)
                    if flipped_obj :
                        child_delta = local_delta * -1
                    else : 
                        child_delta = local_delta
                    if not flipped :
                        child.location -= child_delta
                    else : 
                        child.location -= child_delta * Vector((-1,1,1))

            #counter offset object
            if self.obj.animation_data and self.obj.animation_data.action :
                timings = []
                for fc in self.obj.animation_data.action.fcurves :
                    if fc.data_path in ['location', 'rotation_euler', 'scale']:
                        for kf in fc.keyframe_points :
                            if int(kf.co.x) not in timings :
                                timings.append(int(kf.co.x))
                                
                for t in reversed(timings) :
                    context.scene.frame_set(t)        
                    context.view_layer.update()
                    flipped_obj = is_flipped(self.obj)
                    parent_delta = compute_delta(self.obj.matrix_basis, local_delta)  #translation vector in parent space
                    if flipped_obj :
                        parent_delta *= -1
                    self.obj.location += parent_delta
                    self.obj.keyframe_insert(data_path = 'location', index = -1)
            else :
                flipped_obj = is_flipped(self.obj)
                parent_delta = compute_delta(self.obj.matrix_basis, local_delta)  #translation vector in parent space
                if flipped_obj :
                    parent_delta *= -1
                self.obj.location += parent_delta            
        else : #current frame only
            #offset current frame drawings
            flipped_obj = is_flipped(self.obj)

            for layer in self.obj.data.layers :
                frame = layer.active_frame
                if frame :
                    for stroke in frame.strokes :
                        for point in stroke.points :
                            if flipped_obj or ( not flipped_obj and 'FLIP' in self.obj.keys() and self.obj['FLIP'] == 0):
                                point.co += local_delta
                            else: 
                                point.co -= local_delta

            #offset childs 
            for child, flipped in get_childs(self.obj): 
                #print('CHILD', child.name, flipped)  
                if flipped_obj:
                    child_delta = local_delta * -1   
                else :
                    child_delta = local_delta     
                if not flipped :
                        child.location -= child_delta
                else : 
                    child.location -= child_delta * Vector((-1,1,1))

                if child.animation_data and child.animation_data.action :
                    child.keyframe_insert(data_path = 'location', index = -1) 
            
            parent_delta = compute_delta(self.obj.matrix_basis, local_delta)  #translation vector in parent space
            
            if flipped_obj :
                self.obj.location -= parent_delta
            else : 
                self.obj.location += parent_delta
            if self.obj.animation_data and self.obj.animation_data.action :
                self.obj.keyframe_insert(data_path = 'location', index = -1)

        context.scene.frame_set(self.init_frame)
        return {'FINISHED'}

class RT_OT_OBJGUIDES(Operator):
    """create or removes guides on current collection objects"""
    bl_idname = "rigtools.obj_guides"
    bl_label = "add/remove guides in objects from active collection"
    add : bpy.props.BoolProperty()
    @classmethod
    def poll(cls, context):
        return context.active_object is not None
    
    def create_guides(self, context):
        if 'GUIDE_MAT' not in bpy.data.materials :
            mat = bpy.data.materials.new('GUIDE_MAT')
            bpy.data.materials.create_gpencil_data(mat)
            mat.grease_pencil.color = (0, 1, 0, 1)
            mat.grease_pencil.pass_index = 9
        else : 
            mat = mat = bpy.data.materials['GUIDE_MAT']
            
        for ob in context.collection.all_objects :
            if ob.type == 'GPENCIL' :
                if mat.name not in ob.data.materials.keys() :
                    ob.data.materials.append(mat)
                for i, slot in enumerate(ob.material_slots) :
                    if slot.material is not None and slot.material.name == 'GUIDE_MAT':
                        slot = i                
                if 'TEMP_GUIDE' not in [layer.info for layer in ob.data.layers]  : 
                    guide_layer = ob.data.layers.new('TEMP_GUIDE')
                    guide_layer.lock = True
                    guide_frame = guide_layer.frames.new(0)
                    
                    guide_stroke_1 = guide_frame.strokes.new()
                    guide_stroke_1.material_index = i
                    guide_stroke_1.points.add(2)
                    guide_stroke_1.points[0].co = (0, 0, 0.2)
                    guide_stroke_1.points[1].co = (0, 0, -1)
                    
                    guide_stroke_2 = guide_frame.strokes.new()
                    guide_stroke_2.material_index = i
                    guide_stroke_2.points.add(2)
                    guide_stroke_2.points[0].co = (-0.2, 0, 0)
                    guide_stroke_2.points[1].co = (0.2, 0, 0)
                
    def remove_guides(self, context) :
        for ob in context.collection.all_objects :
            if ob.type == 'GPENCIL':
                for layer in ob.data.layers : 
                    for frame in layer.frames :
                        for stroke in reversed(frame.strokes):
                            mat = ob.data.materials[stroke.material_index]
                            if 'GUIDE_MAT' in mat.name :
                                frame.strokes.remove(stroke)
        for ob in context.collection.all_objects :
            if ob.type == 'GPENCIL' and 'TEMP_GUIDE' in [layer.info for layer in ob.data.layers] :
                 ob.data.layers.remove(ob.data.layers['TEMP_GUIDE'])
        for ob in context.collection.all_objects :
            if ob.type == 'GPENCIL':
                for slot in ob.material_slots: 
                    if 'GUIDE_MAT' in slot.material.name :
                        slot.material = None
        
        bpy.context.view_layer.update()

    def execute(self, context):
        if self.add :
            self.create_guides(context)
        else :
            self.remove_guides(context)
        
        return {'FINISHED'}

def hold_frames(frame_1, frame_2):
    if len(frame_1.strokes) == len(frame_2.strokes):
        for i, stroke_1 in enumerate(frame_1.strokes) :
            stroke_2 = frame_2.strokes[i]
            if len(stroke_1.points) != len(stroke_2.points) :
                return False #not the same amount of points in a stroke    
        for i, stroke_1 in enumerate(frame_1.strokes) :
            stroke_2 = frame_2.strokes[i]    
            for j, point_1 in enumerate(stroke_1.points) :
                point_2 = stroke_2.points[j]
                if point_1.co != point_2.co :
                    return False
        return True #not sculpted but exactly the same
    else :
        return False #not the same amount of strokes

def remove_holds(ob) :
    #print(ob.name)   
    if ob.animation_data and ob.animation_data.action :
        kfs_to_del = []
        
        for fc in ob.animation_data.action.fcurves :
            prev_kf = None
            for kf in fc.keyframe_points :
                if prev_kf :
                    if kf.co.y == prev_kf.co.y and kf.interpolation == 'CONSTANT' and prev_kf.interpolation == 'CONSTANT' :
                        kfs_to_del.append((fc, kf))
                prev_kf = kf    
        #print('kfs : ', len(kfs_to_del))
        for fc, kf in reversed(kfs_to_del) :
            fc.keyframe_points.remove(kf)
            fc.keyframe_points.update()
    if ob.type == 'GPENCIL' :
        gpf_to_del = []
        
        for layer in ob.data.layers :
            if layer.info == 'GPLYR_RGH' :
                pass
            prev_gpf = None
            for gpf in layer.frames :
                if prev_gpf :
                    if hold_frames(gpf, prev_gpf) :
                        gpf_to_del.append((layer,gpf))
                prev_gpf = gpf
        #print('gpfs : ', len(gpf_to_del))
        for layer, gpf in reversed(gpf_to_del) :
            layer.frames.remove(gpf)
            layer.frames.update()

class RT_OT_REMOVE_HOLDS(Operator):
    """supprime les clés de maintien de tous les objets de la collection active"""
    bl_idname = "rigtools.remove_holds"
    bl_label = "Remove hold keyframes from every object of active collection"

    @classmethod
    def poll(cls, context):
        return context.collection is not None
    

    def execute(self, context):
        for ob in context.collection.all_objects :
            remove_holds(ob)
            bpy.context.view_layer.update()
        return {'FINISHED'}


class RT_OT_UNLOCK_ALL(Operator):
    """Débloque tous les layers (GP & transforms) de tous les objets de la collection active"""
    bl_idname = "rigtools.unlock_all"
    bl_label = "Unlock all objects in current collection"

    safe_lock : bpy.props.BoolProperty(default = True)

    @classmethod
    def poll(cls, context):
        return context.collection is not None
    

    def execute(self, context):
        for ob in context.collection.all_objects :
            if ob.type == 'GPENCIL' :
                ob.data.use_autolock_layers = False
                for layer in ob.data.layers :
                    layer.lock = False
            if ob.animation_data and ob.animation_data.action :
                for fc in ob.animation_data.action.fcurves :
                    if fc.data_path == 'rotation_euler' and fc.array_index in [0,2] and self.safe_lock :
                        fc.lock = True
                    else :
                        fc.lock = False
        return {'FINISHED'}
    
class RT_OT_CLONEFLIP_MEMBER(Operator):
    ''' create flippers and flipped object from the active collection objects having the f_suffix '_L' or '_R' and remap constraints,
    flip on x axis flipped objects having 'HOLDER' in its name'''
    bl_idname = "rigtools.cloneflip_member"
    bl_label = "clone a member and flip it automatically"

    re_layout_dict = {
        -1 : '_L$',
        0 : '_C$',
        1 : '_R$', 
        'holder' : 'HOLDER_[L,R]$'
    }
    center_suffix = '_FLIP'


    @classmethod
    def poll(cls, context):
        return context.collection is not None

    def execute(self, context):
        #deselect all objects
        for obj in bpy.context.selected_objects:
            obj.select_set(False)       

        if verbose:print('''---------Sorting objects by suffix---------''')

        # obj_to_flip = []
        lateral_objs_dic_key = {k:[] for k in self.re_layout_dict.keys()}
        root_objs =[]
        roots_dic = {}
        to_flip_dic = {}

        for ob in context.collection.all_objects  :
            # check if object match re dict suffix1 or -1
            for k, layout in self.re_layout_dict.items() :
                if re.search(layout, ob.name) :
                    lateral_objs_dic_key[k].append(ob)
                    #getting parent object not in the list TODO replace by common function get_child_dict                  
                    if k in[1,-1]:
                            # and len([c for c in ob.constraints 
                            # if c.type == 'CHILD_OF' 
                            # and c.target # Shouldn't be necessary but was at some point
                            # and re.search(lateral_objs_dic_key[0], c.target.name)
                            # # and c.target.name not in context.collection.all_objects.keys() #this avoid to buidl same hiearchie level flipper, why? 
                            # ]) > 0 :
                        to_flip_dic[ob.name] = ob
                        for c in ob.constraints :
                                if (c.type == 'CHILD_OF'                              
                                and c.target # Shouldn't be necessary but was at some point
                                and re.search( self.re_layout_dict[0], c.target.name)):
                                    root_objs.append(ob)
                                    roots_dic[c.target.name] = c.target



        if verbose:
            print('root_objs',[obj.name for obj in root_objs])
            print('sorted Dic', lateral_objs_dic_key)

         #TODO external_object_conformation(
                # src_object_dic, 
                # tgt_object_dic,
                # verbose=verbose,
                # function_list=
                    #     [(conform_collections,{}), # Conform collections tree
                    #     (conform_object,{'actions' : ['CREATE',
                    #                                 'LINK',
                    #                                 #'NEW_DATA',                                          
                    #                                 'PROPERTIES',
                    #                                 'PARENT',
                    #                                 'CONSTRAINTS',
                    #                                 'MODIFIERS',
                    #                                 #   'GP_DATA',
                    #                                 'GP_MODIFIERS',
                    #                                 'SHADERS',
                    #                                 'DRIVERS',
                    #                                 ]}),#conform objects existence            

                    # ]
                # re_layout_dict=re_layout_dict,
                # )
        
        if verbose:print('''---------Building src/tgt dictionnary---------''')
        src_dic = roots_dic|to_flip_dic
        #remove already existing  TODO or add them to tgt
        src_dic = {k:v for k,v in src_dic.items() 
                   if regex_flip_name(k,self.re_layout_dict,center_suffix=self.center_suffix) 
                      not in context.scene.collection.all_objects.keys()}

        if verbose:print('''---------Conforming collection---------''')
        conform_collections(src_dic, 
                            context.scene.collection.all_objects, 
                            verbose=verbose,
                            regEx_pattern_flip=self.re_layout_dict,
                            use_current_collection=True,
                            actions=['CREATE','INT_LINK']
                            )

        if verbose:print('''---------Creating flipped objects---------''')
        new_obj_keys = conform_object( src_dic, 
                        {}, 
                        verbose=verbose,
                        actions=['DUPLICATE_FLIP',                                 
                                 'LINK_FLIP',
                                 'LATERAL_PROPERTIES_FLIP',
                                 'PROPERTIES_FLIP',
                                 'NON_NATIVE_DATA_FLIP',
                                 'CONSTRAINTS_FLIP',                              
                                
                                 'BONES_FLIP',
                                 'OBJ_DRIVERS_FLIP'],
                        center_suffix=self.center_suffix,
                        regEx_pattern_flip=self.re_layout_dict,

                        )
        
        if verbose:print('''---------CALLING KEYFRAME MIRRORING---------''')
        
        
        # #SELECTING ALL LATTERALIZED NEW OBJECTS KF for mirroring
        #Deselect all objects
        for obj in bpy.context.selected_objects:
            obj.select_set(False)

        for k in new_obj_keys :
            latind = regex_get_lateral_index(k,self.re_layout_dict)            
            if latind not in [None,0,'HOLDER'] :
                ob = context.scene.objects.get(k)
                if not ob :
                    print('WARNING! object not found:',k) 
                    continue
                if verbose: print('selecting gpframe of ',k)
                ob.select_set(True)
                
        #         #SELECT ALL FCURVE KF
        #         if ob.animation_data and ob.animation_data.action :
        #             for fc in ob.animation_data.action.fcurves :
        #                 for kp in fc.keyframe_points :
        #                     kp.select_control_point = True
        #                     kp.select_left_handle = True
        #                     kp.select_right_handle = True
        #         #SELECT ALL GPFRAME
        #         if ob.type == 'GPENCIL' :
        #             for layer in ob.data.layers :
        #                 for frame in layer.frames :
        #                     frame.select = True
        #CALL OPERATOR rigtools.mirror_keyframes
        for area_iter in context.screen.areas:
            #print(area_iter.type)
            if area_iter.type == "DOPESHEET_EDITOR":
                area_dopesheet = area_iter
                break
        #TEMPORARY SET CONTEXT TO DOPESHEET
        with context.temp_override(area=area_dopesheet):        
            bpy.ops.rigtools.mirror_keyframes('INVOKE_DEFAULT')
            
        return {'FINISHED'}
    
class RT_OT_MIRROR_GPFRAME(Operator):
    '''reverse the order of the selected objects keyframes around a center based on a frame range'''
    bl_idname = "rigtools.mirror_keyframes"
    bl_label = "mirror selected objects keyframes around the center of given frame range"
    bl_options = {'REGISTER', 'UNDO'}
    frame_range_start : IntProperty(name="frame_range_start", default=1)
    frame_range_end : IntProperty(name="frame_range_end", default=17)
    frame_center : IntProperty(name="frame_center", default=9)
    to_draw : BoolProperty(name="to_draw", default=True)

    def draw(self,context):
        if self.to_draw:
            layout = self.layout
            layout.prop(self, "frame_range_start", text="Start")
            layout.prop(self, "frame_range_end", text="End")
            #Calculate center with current frame range
            self.frame_center = int(self.frame_range_start+((self.frame_range_end+1) - self.frame_range_start)/2)
            layout.prop(self, "frame_center", text="Mirror center")

        

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def execute(self, context):
        self.to_draw = False
        
        if verbose:print('''---------Unselecting selected---------''')
        #UNSELECTING ALL KEYFRAME
        for ob in context.collection.all_objects :
            deselect_all_keyframes(ob)

        if verbose:print('''---------Mirroring animation---------''')
        #FOR EACH SELECTED OBJECT
        for ob in context.selected_objects :
            if verbose: print('selecting keyframes of ',ob.name)
            ob.select_set(True)
            #SELECT ALL FCURVE KF IN RANGE

            for fc in ob.animation_data.action.fcurves :
                for kp in fc.keyframe_points :
                    if kp.co[0] >= self.frame_range_start and kp.co[0] <= self.frame_range_end :
                        kp.select_control_point = True
                        kp.select_left_handle = True
                        kp.select_right_handle = True
            #SELECT ALL GPFRAME
            if ob.type == 'GPENCIL' :
                for layer in ob.data.layers :
                    for frame in layer.frames :
                        if frame.frame_number >= self.frame_range_start and frame.frame_number <= self.frame_range_end :
                            frame.select = True
        #Set frame to center
        context.scene.frame_set(self.frame_center)

        for area_iter in context.screen.areas:
            #print(area_iter.type)
            if area_iter.type == "DOPESHEET_EDITOR":
                area_dopesheet = area_iter
                break
        #TEMPORARY SET CONTEXT TO DOPESHEET
        with context.temp_override(area=area_dopesheet):
            #CALL MIRROR OPERATOR
            bpy.ops.action.mirror(type='CFRA')
        return {'FINISHED'}

        
    

def get_suffixes(ob) :
    '''from an object returns its suffix and flipped suffix'''
    if ob.name[-2:] == '_L':
        return 'L', 'R'
    if ob.name[-2:] == '_R':
        return 'R', 'L'
    else :
        return None, None
        
def fc_is_from_array(fcurve) :
    if len([fc for fc in fcurve.id_data.fcurves if fc.data_path == fcurve.data_path]) > 1 :
        return True
    else :
        return False

class RT_OT_OLD_FLIP_ANIMATION(Operator):
    ''' symetrical flip of the animation'''
    bl_idname = "rigtools.old_flip_animation"
    bl_label = " symetrical flip of the animation"

    re_layout_dict = {
        -1 : '_L$',
        0 : '_C$',
        1 : '_R$', 
        'holder' : 'HOLDER_[L,R]$'
    }
    def execute(self,context):
        cur_frame = context.scene.frame_current
        for ob in bpy.context.collection.all_objects : 
            print('--------')
            print(ob.name)
            suffix, f_suffix = get_suffixes(ob)
            #'''FLIP LATERAL OBJECTS'''
            if suffix is not None :
                print('+')
                if ob.name[:-1]+f_suffix in bpy.data.objects.keys() :
                    f_ob = bpy.data.objects[ob.name[:-1]+f_suffix]
                    print(f_ob.name)
                    # FLIP TRANSFORMS
                    if ob.animation_data and ob.animation_data.action :
                        for fc in ob.animation_data.action.fcurves :
                            if not fc_is_from_array(fc) :
                                fc_index = -1
                            else :
                                fc_index = fc.array_index
                            for kf in fc.keyframe_points :
                                if kf.select_control_point :
                                    f_ob.keyframe_insert(data_path = fc.data_path, index= fc_index, frame = cur_frame)
                                    print('copy kf')
                                    for ffc in f_ob.animation_data.action.fcurves :
                                        if ffc.data_path == fc.data_path and ffc.array_index == fc.array_index :
                                            new_kf = get_now_kf(cur_frame, ffc)
                                            new_kf.co.y = kf.co.y
                                            new_kf.select_control_point = False
                                            print('co ok')
                                            break
                    # FLIP GP FRAMES
                    if ob.type == 'GPENCIL' :
                        for layer in ob.data.layers :
                            f_layer = f_ob.data.layers[layer.info]
                            
                            for frame in layer.frames :
                                if frame.select :
                                    new_frame = f_layer.frames.copy(frame)
                                    new_frame.select = False
                                    move_gp_frame(f_layer, new_frame, cur_frame)
                                    print('copy_frame')
            #'''FLIP CENTRAL OBJECTS'''                          
            elif ob.name[-2:] == '_C':                 
                print('__C__')
                # FLIP TRANSFORMS
                if ob.animation_data and ob.animation_data.action :
                    for fc in ob.animation_data.action.fcurves :
                        print('CHECK : ', fc.data_path,fc.array_index )
                        if fc_is_from_array(fc) :
                            fc_index = fc.array_index             
                        else :
                            fc_index = -1
                        for kf in fc.keyframe_points :
                            if kf.select_control_point :
                                ob.keyframe_insert(data_path = fc.data_path, index= fc_index, frame = cur_frame)
                                new_kf = get_now_kf(cur_frame, fc)
                                new_kf.select_control_point = False
                                if (
                                   (fc.data_path == 'location' and fc.array_index == 0) 
                                   or (fc.data_path == 'rotation_euler' and fc.array_index == 1)
                                   ) :
                                    print('++++++flip : ', fc.data_path,fc.array_index )
                                    print(kf.co.y)
                                    new_kf.co.y = kf.co.y * -1
                                    print(new_kf.co.y)
                                else :
                                    print('reset?')
                                    new_kf.co.y = kf.co.y
                # FLIP GP FRAMES
                if ob.type == 'GPENCIL' :
                    print('GP flip')
                    for layer in ob.data.layers :
                        print(layer.info)
                        framenumbers = []
                        for frame in layer.frames :
                            framenumbers.append(frame.frame_number)
                            
                        for frame_number in framenumbers :
                            frame = get_now_gpframe(frame_number, layer)
                            if frame.select :
                                print(frame.frame_number)
                                new_frame = layer.frames.copy(frame)
                                new_frame = move_gp_frame(layer, new_frame, cur_frame)
                                new_frame.select = False
                                frame = get_now_gpframe(frame_number, layer)
                                '''for stroke in new_frame.strokes :
                                    print(stroke)
                                    for point in stroke.points:
                                        
                                        point.co[0] *= -1'''
                            
                                for i, stroke in enumerate(frame.strokes) :
                                    new_stroke = new_frame.strokes[i]
                                    if not ob.material_slots[stroke.material_index].material.grease_pencil.show_stroke :
                            
                                        for j, point in enumerate(reversed(stroke.points)) :
                                            index = j + int(len(stroke.points)/2)
                                            if index >= len(stroke.points) :
                                                index -= len(stroke.points)
                                            new_point = new_stroke.points[index]
                                            new_point.co = point.co * Vector((-1, 1, 1))
                                    else :
                                        for j, point in enumerate(stroke.points) : #reversed ?
                                            new_point = new_stroke.points[j]                        
                                            for attr in dir(point) :
                                                try :                            
                                                    setattr(new_point, attr, getattr(point, attr))                  
                                                except :
                                                    pass
                                            new_point.co = point.co * Vector((-1, 1, 1))
                        
                                            
        #still need to kind of refresh by frame change
        bpy.context.scene.frame_set(bpy.context.scene.frame_current +1) 
        bpy.context.scene.frame_set(bpy.context.scene.frame_current -1) 
        bpy.context.view_layer.update()
        return {'FINISHED'}

class RT_OT_FLIP_ANIMATION(Operator):
    ''' symetrical flip of the animation'''
    bl_idname = "rigtools.flip_animation"
    bl_label = " symetrical flip of the animation"
    bl_options = {'UNDO'}
    re_layout_dict = {
        -1 : '_L$',
        0 : '_C$',
        1 : '_R$', 
        'holder' : 'HOLDER_[L,R]$'
    }


    # @classmethod
    # def poll(cls, context):
    #     return context.collection is not None
    def execute(self, context):
        if verbose:print('''---------Flipping animation---------''')


        start_frame,end_frame,obj_keys = get_dopesheet_selected(verbose = verbose) 
       
        # INITIAL CHECKS
        if len(obj_keys)==0:
            self.report({'ERROR'}, "No keyframe selected in the dopesheet!")
            return {'CANCELLED'}
        
        if verbose:print('----from source frame %s to %s '%(start_frame,end_frame))

        tgt_starting_frame = bpy.context.scene.frame_current
        if verbose:print('----to target frame %s to %s'%(tgt_starting_frame,tgt_starting_frame+(end_frame-start_frame)))

        
        if (tgt_starting_frame in range(start_frame,end_frame+1) 
            or tgt_starting_frame+(end_frame-start_frame) in range(start_frame,end_frame+1)
            ):
            self.report({'ERROR'}, "Target and cible frames ranges cannot overlap!")
            return {'CANCELLED'}
        
        # FLIP
        for k in obj_keys : 
            ob = bpy.data.objects[k]
            if verbose:print('-------- FLIPPING %s : '%ob.name)
            flip_animation(ob,
                           regEx_pattern_flip=self.re_layout_dict, 
                           src_range=[start_frame,end_frame+1],
                           tgt_starting_frame = tgt_starting_frame,                           
                           verbose = verbose,
                           force_gpframe_creation=False)
            
        # REFRESH
        bpy.context.scene.frame_set(bpy.context.scene.frame_current +1) 
        bpy.context.scene.frame_set(bpy.context.scene.frame_current -1) 
        bpy.context.view_layer.update()

        return {'FINISHED'}
    
        



class RT_OT_ADD_HIDE_HELPERS_MOD(Operator):
    '''adds a modifier to every GP in the active collection, disabling in render materials with pass index at 9 (helpers)'''
    bl_idname = "rigtools.add_hide_helpers_mod"
    bl_label = "hide helper materials in render"

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context):
        master = context.object
        if 'CTRL_VISIBILITY' in master.keys() :
            for ob in bpy.context.selected_objects :
                if ob.type == 'GPENCIL':
                    if ob.name + '_HIDE_HELPERS' not in ob.grease_pencil_modifiers.keys():
                        mod = ob.grease_pencil_modifiers.new(ob.name + '_HIDE_HELPERS', 'GP_OPACITY')
                        mod.show_viewport = True
                        mod.show_render = True 
                        mod.factor = 0.0
                        mod.pass_index = 9 

                        if ob.animation_data == None :
                            ob.animation_data_create()
                        dri = ob.animation_data.drivers.new(data_path = 'grease_pencil_modifiers["' + str(mod.name)+'"].show_viewport', index = -1).driver
                        dri.type = 'SCRIPTED'
                        var = dri.variables.new()
                        var.name = 'CTRL_VISIBILITY'
                        var.targets[0].id = master
                        var.targets[0].data_path = '["CTRL_VISIBILITY"]'
                        dri.expression = '1 - CTRL_VISIBILITY'
        else :
            self.report({'ERROR'}, 'No CTRL_VISIBILITY property found on active object')
            return {'CANCELLED'}
        return {'FINISHED'}
    
class RT_OT_ADD_STROKE_THICK_MOD(Operator):
    '''adds a modifier to every GP in the active collection, driving line thickness on master custom property'''
    bl_idname = "rigtools.add_stroke_thick_mod"
    bl_label = "add and drive stroke thickness modifier"

    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context):
        master = context.object
        if 'CTRL_THICKNESS' in master.keys() :
            for ob in bpy.context.selected_objects :
                if ob.type == 'GPENCIL':
                    if ob.name + '_STROKE_THICKNESS' not in ob.grease_pencil_modifiers.keys():
                        mod = ob.grease_pencil_modifiers.new(ob.name + '_STROKE_THICKNESS', 'GP_THICK')
                        mod.show_viewport = True
                        mod.show_render = True
                        if ob.animation_data == None :
                            ob.animation_data_create()
                        dri = ob.animation_data.drivers.new(data_path = 'grease_pencil_modifiers["' + str(mod.name)+'"].thickness_factor', index = -1).driver
                        dri.type = 'SCRIPTED'
                        var = dri.variables.new()
                        var.name = 'CTRL_THIK'
                        var.targets[0].id = master
                        var.targets[0].data_path = '["CTRL_THICKNESS"]'
                        dri.expression = 'CTRL_THIK'
        else :
            self.report({'ERROR'}, 'No CTRL_THICKNESS property found on active object')
            return {'CANCELLED'}
        return {'FINISHED'}
    
def add_distribute_points(init_coords, number):
    coords = init_coords.copy()
    for i in range(number) : 
        if i >= len(init_coords) - 1 : 
            coords = add_distribute_points(coords, number - i)
            break
        coords.insert(2*i + 1 , (init_coords[i] + init_coords[i+1])/2)
    return coords

class RT_OT_DEL_SMALL_STROKES(Operator):
    '''force gp interpolability of selected frames of selected objects'''
    bl_idname = "rigtools.del_small_strokes"
    bl_label = "remove with not enough points"
    bl_options = {'UNDO', 'REGISTER'}
    MIN_LENGTH : bpy.props.IntProperty(default = 2)
    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context) :
        for ob in context.selected_objects :
            if ob.type == 'GPENCIL':
                if ob.name + '_HIDE_HELPERS' not in ob.modifiers.keys():
                    mod = ob.grease_pencil_modifiers.new(ob.name + '_HIDE_HELPERS', 'GP_OPACITY')
                    mod.show_viewport = False
                    mod.show_render = True
                    mod.factor = 0.0
                    mod.pass_index = 9 
        return {'FINISHED'}
    


class RT_OT_ADD_HIDE_HELPERS_MOD(Operator):
    '''adds a modifier to every GP in the active collection, disabling in render materials with pass index at 9 (helpers)'''
    bl_idname = "rigtools.add_hide_helpers_mod"
    bl_label = "hide helper materials in render"
    object_name : bpy.props.StringProperty()
    master_name : bpy.props.StringProperty()
    

    def execute(self, context):
        if self.master_name in bpy.data.objects.keys():
            master = bpy.data.objects[self.master_name]
        else :
            master = context.object
        
        if self.object_name in bpy.data.objects.keys():
            obs = [bpy.data.objects[self.object_name]]
        else :
            obs = bpy.context.selected_objects

        if 'CTRL_VISIBILITY' in master.keys() :
            for ob in obs :
                if ob.type == 'GPENCIL':
                    if ob.name + '_HIDE_HELPERS' not in ob.grease_pencil_modifiers.keys():
                        mod = ob.grease_pencil_modifiers.new(ob.name + '_HIDE_HELPERS', 'GP_OPACITY')
                        mod.show_viewport = True
                        mod.show_render = True 
                        mod.factor = 0.0
                        mod.pass_index = 9 

                        if ob.animation_data == None :
                            ob.animation_data_create()
                        dri = ob.animation_data.drivers.new(data_path = 'grease_pencil_modifiers["' + str(mod.name)+'"].show_viewport', index = -1).driver
                        dri.type = 'SCRIPTED'
                        var = dri.variables.new()
                        var.name = 'CTRL_VISIBILITY'
                        var.targets[0].id = master
                        var.targets[0].data_path = '["CTRL_VISIBILITY"]'
                        dri.expression = '1 - CTRL_VISIBILITY'
        else :
            self.report({'ERROR'}, 'No CTRL_VISIBILITY property found on active object')
            return {'CANCELLED'}
        return {'FINISHED'}
    
class RT_OT_ADD_STROKE_THICK_MOD(Operator):
    '''adds a modifier to every GP in the active collection, driving line thickness on master custom property'''
    bl_idname = "rigtools.add_stroke_thick_mod"
    bl_label = "add and drive stroke thickness modifier"
    object_name : bpy.props.StringProperty()
    master_name : bpy.props.StringProperty()
    

    def execute(self, context):
        if self.master_name in bpy.data.objects.keys():
            master = bpy.data.objects[self.master_name]
        else :
            master = context.object
        
        if self.object_name in bpy.data.objects.keys():
            obs = [bpy.data.objects[self.object_name]]
        else :
            obs = bpy.context.selected_objects



        if 'CTRL_THICKNESS' in master.keys() :
            for ob in obs :
                if ob.type == 'GPENCIL':
                    if ob.name + '_STROKE_THICKNESS' not in ob.grease_pencil_modifiers.keys():
                        mod = ob.grease_pencil_modifiers.new(ob.name + '_STROKE_THICKNESS', 'GP_THICK')
                        mod.show_viewport = True
                        mod.show_render = True
                        mod.pass_index = 8
                        mod.invert_material_pass = True
                        if ob.animation_data == None :
                            ob.animation_data_create()
                        dri = ob.animation_data.drivers.new(data_path = 'grease_pencil_modifiers["' + str(mod.name)+'"].thickness_factor', index = -1).driver
                        dri.type = 'SCRIPTED'
                        var = dri.variables.new()
                        var.name = 'CTRL_THIK'
                        var.targets[0].id = master
                        var.targets[0].data_path = '["CTRL_THICKNESS"]'
                        dri.expression = 'CTRL_THIK'
        else :
            self.report({'ERROR'}, 'No CTRL_THICKNESS property found on active object')
            return {'CANCELLED'}
        return {'FINISHED'}
    
def add_distribute_points(init_coords, number):
    coords = init_coords.copy()
    for i in range(number) : 
        if i >= len(init_coords) - 1 : 
            coords = add_distribute_points(coords, number - i)
            break
        coords.insert(2*i + 1 , (init_coords[i] + init_coords[i+1])/2)
    return coords

def get_stroke_index(target_stroke, frame):
    for i, stroke in enumerate(frame.strokes):
        if stroke == target_stroke :
            return i, stroke



class RT_OT_SWITCH_STROKES(Operator):
    '''force gp interpolability of selected frames of selected objects'''
    bl_idname = "rigtools.switch_strokes"
    bl_label = "switch stroke indices"
    bl_options = {'UNDO', 'REGISTER'}
    

    @classmethod
    def poll(cls, context):
        return context.object is not None and context.object.type == 'GPENCIL' and context.mode == 'EDIT_GPENCIL'

    def execute(self, context) :
        layer = context.object.data.layers.active
        if layer :
            frame = layer.active_frame
            if frame :
                strokes = [stroke for stroke in frame.strokes if stroke.select]
                if len(strokes) == 2 :
                    index_1, stroke_1 = get_stroke_index(strokes[0], frame)
                    index_2, stroke_2 = get_stroke_index(strokes[1], frame)
                    print('index_1 : ', index_1, '---- index_2', index_2)
                    delta = index_2 - index_1
                    stroke_1.select = False
                    for i in range(delta -1) :
                        bpy.ops.gpencil.stroke_arrange(direction='DOWN')
                    stroke_2.select = False
                    stroke_1.select = True
                    for i in range(delta):
                        bpy.ops.gpencil.stroke_arrange(direction='UP')
                    stroke_2.select = True

                else :
                    self.report({'INFO'}, 'you have to select exactly 2 strokes to switch indexes')
        return {'FINISHED'}
    
class RT_OT_DEL_SMALL_STROKES(Operator):
    '''force gp interpolability of selected frames of selected objects'''
    bl_idname = "rigtools.del_small_strokes"
    bl_label = "remove with not enough points"
    bl_options = {'UNDO', 'REGISTER'}
    MIN_LENGTH : bpy.props.IntProperty(default = 2)
    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context) :
        for ob in context.selected_objects :
            if ob.type == 'GPENCIL':
                for layer in ob.data.layers :
                    if not layer.lock :
                        for frame in layer.frames :
                            strokes_to_del = []
                            for stroke in frame.strokes :
                                if len(stroke.points) <= self.MIN_LENGTH :
                                    strokes_to_del.append(stroke)
                            for stroke in strokes_to_del :
                                frame.strokes.remove(stroke)
        return {'FINISHED'}

class RT_OT_UPDATE_STROKES(Operator):
    '''refresh bugged strokes of current collection objects'''
    bl_idname = "rigtools.update_strokes"
    bl_label = "update strokes"
    bl_options = {'UNDO'}

    def execute(self, context) :
        for ob in context.collection.all_objects :
            if ob.type == 'GPENCIL' and not ob.library:
                for layer in ob.data.layers :
                        for frame in layer.frames :
                            for stroke in frame.strokes :
                                stroke.points.update()
        return {'FINISHED'}

class RT_CLIPBOARD(bpy.types.PropertyGroup):
    transforms = {}


class RT_OT_COPY_OBJ_TRANSFORMS(Operator):
    '''copy selected object transforms to clipboard'''
    bl_idname = "rigtools.copy_obj_transforms"
    bl_label = "Copy transforms to clipboard"

    def execute(self, context) :
        ob = context.object
        scene = context.scene

        scene.clipboard.transforms = {}
        init_fr = scene.frame_current
        
        for i in range(scene.frame_start, scene.frame_end +1):
            scene.frame_set(i)
            context.view_layer.update()
            scene.clipboard.transforms[i] = Matrix(ob.matrix_world)

        #print(context.scene.clipboard.transforms)    
        scene.frame_set(init_fr)

        return {'FINISHED'}
    
class RT_OT_PASTE_OBJ_TRANSFORMS(Operator):
    '''paste clip board transforms to selected objects'''
    bl_idname = "rigtools.paste_obj_transforms"
    bl_label = "paste transforms from clipboard"
    bl_options = {'UNDO'}

    def execute(self, context):
        ob = context.object
        scene = context.scene
        
        transforms = scene.clipboard.transforms
        init_fr = scene.frame_current

        for i in transforms.keys():
            scene.frame_set(i)
            context.view_layer.update()
            #print(ob.matrix_world == transforms[i])
            if ob.matrix_world != transforms[i] :
                snap_parented_object(ob, transforms[i])
                print('SNAP!', i)
        scene.frame_set(init_fr)

        return {'FINISHED'}

class RT_OT_FORCE_INTERPO(Operator):
    '''force gp interpolability of selected frames of selected objects'''
    bl_idname = "rigtools.force_interpo"
    bl_label = "force gp interpolability"
    bl_options = {'UNDO', 'REGISTER'}
    MAX_DIFF : bpy.props.IntProperty(default = 5)
    @classmethod
    def poll(cls, context):
        return context.object is not None

    def execute(self, context):
        errors = []
        for ob in context.selected_objects :
            if ob.type == 'GPENCIL':
                for layer in ob.data.layers :
                    
                    layer_error = False
                    framedict = {}
                    for frame in layer.frames : #iterate through frames
                        if frame.select :
                            framedict[frame] = len(frame.strokes)
                    
                    if len(framedict) > 0 :
                        mean = (sum(framedict.values())/len(framedict))
                        for frame in framedict.keys() :
                            if len(frame.strokes) != mean :
                                layer_error = True
                                errors.append(('stroke number : ', ob.name, layer.info, 'frame : ', frame.frame_number))
                                break
                        if not layer_error :
                            
                            for i in range(len(list(framedict.keys())[0].strokes)) : #iterate through strokes
                                strokedict = {}
                                ctrl_mat = None
                                for frame in framedict.keys() :
                                    if ctrl_mat and ob.material_slots[frame.strokes[i].material_index].material.name != ctrl_mat :
                                        errors.append(('stroke materials are not corresponding (use of arrange tool ?)', ob.name, layer.info))
                                        framedict.pop(frame)
                                        break
                                    
                                    ctrl_mat = ob.material_slots[frame.strokes[i].material_index].material.name
                                    
                                    strokedict[frame.strokes[i]] = len(frame.strokes[i].points)
                                if len(strokedict) > 0 :
                                    liste = list(strokedict.values())
                                    maximum = max(liste)
                                    for frame in framedict.keys() :
                                        init_len_points = len(frame.strokes[i].points)
                                        diff = maximum - init_len_points
                                        if init_len_points != maximum :
                                            if diff < self.MAX_DIFF :

                                                init_coords = [point.co for point in frame.strokes[i].points]
                                                coords = add_distribute_points(init_coords, diff )

                                                frame.strokes[i].points.add(diff, pressure = 1, strength = 1)     
                                                for j, co in enumerate(coords) :
                                                    frame.strokes[i].points[j].co = co     
                                                                        
                                            
                                            else : errors.append(('difference of points number above MAX_DIFF  ', ob.name, layer.info, frame.frame_number))
        if len(errors) > 0:
            self.report({'INFO'},'some errors raised, see console for more info' )
            print('errors : ') 
            for err in errors :
                print(err)
        else :
            print('no errors everything ok !')
        return {'FINISHED'}

def get_selected_points(context):
    if context.active_object.type != 'GPENCIL':
        return []
    gp = context.active_object.data
    return [p
        for lr in gp.layers
            if not lr.lock and not lr.hide  #Respect layer locking and visibility
                for fr in ([fr for fr in lr.frames if fr.select or fr == lr.active_frame] if gp.use_multiedit else [lr.active_frame])    #Respect multiframe editing settings
                    for s in fr.strokes
                        if s.select
                            for p in s.points
                                if p.select]   

class hardnessOperator(bpy.types.Operator):
    """Middle mouse to adjust selected strokes' hardness.
Hold CTRL to adjust selected points' pressure(radius) instead.
Hold SHIFT to adjust selected points' strength instead.
Left click to apply"""
    
    bl_idname = "stroke.hardness"
    bl_label = "Stroke Hardness"
    bl_options = {'REGISTER', 'UNDO'}
    selected_points = []
    @classmethod
    def poll(self, context):
        return (context.mode == 'SCULPT_GPENCIL' or context.mode == 'EDIT_GPENCIL')
    
    def modal(self, context, event):
        if event.type == "WHEELUPMOUSE" or event.type == "WHEELDOWNMOUSE":
            incr = -0.01 if event.type == "WHEELDOWNMOUSE" else 0.01

            if event.shift:
                incr *= 2
                for p in self.selected_points:
                    p.strength += incr
            elif event.ctrl:
                incr *= 10
                for p in self.selected_points:
                    p.pressure += incr
            else:
                gp =  context.active_object.data
                for lr in gp.layers:
                    if not lr.lock and not lr.hide:
                        frame_list = [fr for fr in lr.frames if fr.select] if gp.use_multiedit else [lr.active_frame]
                        for fr in frame_list:
                            for s in fr.strokes:
                                if s.select:
                                    s.hardness += incr
                                    context.area.header_text_set("Hardness: %.4f" % s.hardness)
                    
        elif event.type == "LEFTMOUSE":
            context.area.header_text_set(None)
            context.window.cursor_modal_restore()
            return {'FINISHED'}
        
        return {'RUNNING_MODAL'}    

    def execute(self, context):
        self.selected_points = get_selected_points(context)    
        context.window.cursor_modal_set("SCROLL_Y")
        context.window_manager.modal_handler_add(self)
        
        return {'RUNNING_MODAL'}
    
    def cancel(self, context):
        context.area.header_text_set(None)
        context.window.cursor_modal_restore()