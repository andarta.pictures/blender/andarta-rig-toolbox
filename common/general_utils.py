import re
import bpy
from .anim_utils import copy_driver, create_anim_data, fc_is_from_array, get_driver, get_fc_array_index
from .gp_utils import sync_layers, sync_materials
from .regex_utils import regex_flip_name, regex_get_lateral_index
from mathutils import Matrix

def reload_scene(scene):
    '''corrects a bug when importing from linked scene, objects transforms are not correctly evaluated unless you switch once to it
    args : scene = the source scene to reload'''
    current_scene = bpy.context.scene
    bpy.context.window.scene = scene
    bpy.context.view_layer.update()
    bpy.context.window.scene = current_scene


### --------------------------CONFORMATION TOOLS-------------------------- ###
def external_object_conformation(src_object_dic, tgt_object_dic,verbose=True,function_list=None,**kwargs):
    """Given a source object dictionary and a target object dictionary, conform the target objects to the source objects
    in a step by step fashion to avoid foreign data error
    args : src_object_dic = a dictionary of source objects
           tgt_object_dic = a dictionary of target objects
           verbose = print the function debug info
           funct_list = a list of functions to apply to the target object"""

    if function_list is None :
        function_list = [
            #(function,kawrgs**) 
            (conform_collections,{}), # Conform collections tree
            (conform_object,{'actions' : ['CREATE',
                                          'LINK',
                                          #'NEW_DATA',                                          
                                          'PROPERTIES',
                                          'NON_NATIVE_DATA',
                                        #   'IK_PARTS',
                                          'PARENT',
                                          'ACTIONS',
                                          'CONSTRAINTS',
                                          
                                          'MODIFIERS',
                                          'GP_DATA',
                                          'GP_MODIFIERS',
                                          'SHADERS',
                                          'OBJ_DRIVERS',
                                          'GP_DRIVERS',
                                          'BONES_CONSTRAINTS',
                                          
                                          # linked to addon Ewilans Quest Rig Setup n Control
                                           ]}),#conform objects existence            

        ]

    for func_set in function_list:
        func_set[0](src_object_dic, tgt_object_dic, verbose, **func_set[1]|kwargs)
        
    
    return None

def conform_collections(src_object_dic, tgt_object_dic, verbose=True, 
                        actions = ['CREATE',
                                   'EXT_LINK'], 
                        **kwargs):
    """Given a source object dictionary and a target object dictionary, conform the target collection tree to the source tree
    args : src_object_dic = a dictionary of source objects
            tgt_object_dic = a dictionary of target objects
            verbose = print the function debug info
            actions = a list of actions to perform on the target collection tree:
                CREATE : create missing collections
                EXT_LINK : link collections when src is external
                INT_LINK : link collections when src is internal
            kwargs :
                use_current_collection : bool = use the current collection as root for the target collection tree
                regEx_pattern_flip : dic = a regular expression pattern dic to create opposite lateral collection from source

    """
    # store kwarg variables 
    use_current_col = kwargs.get('use_current_collection',False)
    regEx_pattern_flip = kwargs.get('regEx_pattern_flip',None)


    #get source mandatory collections:
    source_col = None
    src_col = {}
    for k,src_object in src_object_dic.items() :
        src_col |= get_object_collections(src_object, recursiv_dic = src_col, get_elders = True  )#not use_current_col
    #get existing collections
    sel_col = {}
    for k,sel_object in tgt_object_dic.items() :
        sel_col |= get_object_collections(sel_object, recursiv_dic = sel_col, get_elders = True)

    newly_created_keys = []
    for a in actions :
        for k,c1 in src_col.items() :
            if not regEx_pattern_flip : 
                cible_name = k
            else:
                cible_name = regex_flip_name(k, regEx_pattern_flip)
            match a :
                case 'CREATE' :
                    #create missing collections                  

                    c2 = sel_col.get(cible_name, None)
                    if not c2 :
                        c2 = bpy.data.collections.get(cible_name)# doesnt work as it get the linked collection
                        if use_current_col:
                            # check if it is source collection
                            src_col_parents = get_collection_parents(c1,list(src_col.values()) )
                            if len(src_col_parents) == 0 :
                                source_col = c1
                                if verbose : print(f"Collection {cible_name} is asset root, replaced by current collection here")
                                continue

                    if not c2 or not bpy.context.scene.user_of_id(c2):
                        if verbose : print(f"Collection {cible_name} doesn't exist in the target scene, creating it")
                        c2 = bpy.data.collections.new(cible_name)
                        
                                                
                        newly_created_keys.append(cible_name)
                    #sync settings            
                    sync_collections(c1, c2)
                case 'EXT_LINK' :
                    #link collections
                    src_col_parents = get_collection_parents(c1,list(src_col.values()) )
                    #get the scene collection by name
                    if use_current_col:
                        #replace sourcecol  in src_col_parents by current collection
                        if source_col in src_col_parents :
                            if verbose : print(f"Forcing parenting to current collection")
                            src_col_parents = [bpy.context.view_layer.active_layer_collection.collection]


                    if len(src_col_parents) == 0 : # avoid unlinked collections
                        if c1 == source_col :
                            if verbose : print(f"Collection {c1.name} is linked root collection: DO NOT LINK IT")
                            continue
                        if verbose : print(f"Collection {c1.name} is linked to the root collection")
                        if use_current_col:
                            src_col_parents = [bpy.context.view_layer.active_layer_collection.collection]
                        else:
                            src_col_parents = [bpy.context.scene.collection]

                    for c2 in src_col_parents :
                        if not regEx_pattern_flip : 
                            prt_cible_name = c2.name
                            child_cible_name = k
                        else:
                            prt_cible_name = regex_flip_name(c2.name, regEx_pattern_flip)
                            child_cible_name = regex_flip_name(k, regEx_pattern_flip)

                        parent_col = bpy.data.collections.get(prt_cible_name, None)
                        #parent_col = tgt_object_dic.scene.collection.children.get(src_parent.name, None)
                        if parent_col:#  and not bpy.context.scene.user_of_id(c2): 
                            try:       
                                tgt_collection = bpy.data.collections.get(child_cible_name)
                                parent_col.children.link(tgt_collection)
                            except:
                                if verbose : print(f"Collection {child_cible_name} is already linked to {parent_col.name}")
                        else:
                                if verbose : print(f"Collection {child_cible_name} has to be linked to scene collection")                
                                try:       
                                    tgt_collection = bpy.data.collections.get(child_cible_name)
                                    bpy.context.scene.collection.children.link(tgt_collection)                                  
                                except:
                                    if verbose : print(f"Collection {child_cible_name} is already linked to SCENE COLLECTION")
                case 'INT_LINK' :
                    'link collections inside the same scene to the source parent collection'
                    if cible_name in newly_created_keys :
                        src_col_parents = get_collection_parents(c1 )

                        if len(src_col_parents) == 0 : # avoid unlinked collections
                            if verbose : print(f"Collection {cible_name} has to be linked to the root collection")
                            if use_current_col:
                                src_col_parents = [bpy.context.view_layer.active_layer_collection.collection]
                            else:
                                src_col_parents = [bpy.context.scene.collection]
                        tgt_collection = bpy.data.collections.get(cible_name)
                        for c2 in src_col_parents :
                            prt_cible_name = regex_flip_name(c2.name, regEx_pattern_flip)
                            parent_col = bpy.data.collections.get(prt_cible_name, None)
                            parent_col.children.link(tgt_collection)
    

                        continue
                    pass

        
    return newly_created_keys

def conform_object(src_objects_dic, tgt_objects_dic, verbose=True, actions = [],**kwargs):
    """Given a source object and a target object, conform the target object to the source object
    args : src_object = the source object
           tgt_object = the target object
           verbose = print the function debug info
           actions = a list of actions to perform on the target object in the declared order.
           actions are performed one at a time on each objects
           actions can be :
                'CREATE' : create the target object if it doesn't exist
                'NEW_DATA' : create the target object data if it doesn't exist
                'PROPERTIES' : copy the source object properties to the target object
                'LINK' : link the target object to the source object parents
                'PARENT' : parent the target object to the source object
                'CONSTRAINTS' : copy the source object constraints to the target object
                'MODIFIERS' : copy the source object modifiers to the target object
                'GP_MODIFIERS' : copy the source object grease pencil modifiers to the target object
                'SHADERS' : copy the source object shaders to the target 
                'GP_DATA' : copy the source object grease pencil data to the target object
                'DRIVERS' : copy the source object drivers to the target object
                'ACTIONS' : copy the source object actions to the target object
                'DUPLICATE_FLIP' : duplicate the target object and flip its name according to the regex pattern
                'LINK_FLIP' : link the target object to the source object parents
                'CONSTRAINTS_FLIP' : flip the target object constraints
                'BONES_CONSTRAINTS' : copy the source object armature bones constraints to the target object
                'IK_PARTS' : copy the source object ik parts to the target object (linked to Anfarta's rig setup n control addon)
                'PROPERTIES_FLIP': make sure that pointersProperties, if they are lateralised, are pointing to the right side
             kwargs :
                regEx_pattern_flip : a regex pattern dic to flip the tgt object name
                center_suffix : a suffix to add to the center objects names   
                non_native_gp_data : a list of non native grease pencil data to copy                        
            return : newly_created_keys = a list of newly created objects names
                """
    # store kwarg variables 
    regEx_pattern_flip = kwargs.get('regEx_pattern_flip',None)
    center_suffix = kwargs.get('center_suffix','')
    non_native_gp_data = kwargs.get('non_native_gp_data',['ik_parts',
                                                          'ik_root',
                                                          'quick_selects',
                                                          'gpmdatas',])
    lat_properties = kwargs.get('lat_properties',['FLIP_DRAWINGS'])
    

    newly_created_keys = []
    for a in actions :
        if verbose:print('Conforming :', a)
        for k in src_objects_dic.keys() :
            src_obj = src_objects_dic.get(k, None)

            if not regEx_pattern_flip : 
                cible_name = k
            else:
                cible_name = regex_flip_name(k, regEx_pattern_flip, center_suffix)
            tgt_obj = tgt_objects_dic.get(cible_name, None)

            match a :
                case 'CREATE' :

                    if not tgt_obj :
                        
                            # new_ob.pose.bones.clear()
                        #copy object
                        new_ob = duplicate_object(src_obj)

                        #change its name for local
                        new_ob.name = src_obj.name
                        if src_obj.data is not None :
                            new_ob.data.name = src_obj.data.name

                        new_ob.empty_display_size = src_obj.empty_display_size
                        new_ob.empty_display_type = src_obj.empty_display_type
                        

                        # Clear animation data 
                        # TODO This sometime implie to delete data rebuild later. 
                        # could be more efficient not to do it
                        if new_ob.animation_data :
                            new_ob.animation_data_clear()
                            new_ob.animation_data_create()
                        elif src_obj.data and src_obj.data.animation_data :
                            if verbose:print(f"Adding empty animation data on {src_obj.data.name}")
                            # new_ob.data.animation_data_clear()
                            new_ob.data.animation_data_create()

                        if new_ob.type == 'GPENCIL':
                            new_ob.data.use_autolock_layers = False
                            for layer in new_ob.data.layers :
                                for frame in reversed(layer.frames) :
                                    layer.frames.remove(frame)
                        elif new_ob.type == 'ARMATURE':
                            if verbose:print(f"Conforming armature {new_ob.name} ")
                            # #remove armature
                            # # bpy.data.armatures.remove(new_ob.data)
                            # conform_armature(src_obj, new_ob, verbose, actions = actions,**kwargs)
                            # continue
                        

                        #copy visibility in viewport and render
                        new_ob.hide_viewport = src_obj.hide_viewport
                        new_ob.hide_render = src_obj.hide_render
                        if new_ob.name != src_obj.name :
                            print('WARNING : object name %s different from source object name %s'%(new_ob.name, src_obj.name))
                        tgt_objects_dic[k] = new_ob
                        newly_created_keys.append(k)
                case 'NEW_DATA' : # USELESS: handled before
                    if k in newly_created_keys :
                        if src_obj.data is not None :
                            tgt_obj.data = src_obj.data.copy()
                            tgt_obj.data.name = src_obj.data.name
                        if tgt_obj.animation_data :
                            #avoid copying existing animation data
                            tgt_obj.animation_data_clear()

                case 'PROPERTIES' :
                    for src_prop_name in src_obj.keys() :
                        if k in newly_created_keys or src_prop_name not in tgt_obj.keys(): #missing property
                            source_prop = src_obj.get(src_prop_name)
                            if verbose:print('setting property %s on %s'%(src_prop_name, tgt_obj.name))                            
                            
                            tgt_prop = conform_prop_data(source_prop,
                                                         off_v='>>>')                                                
                            tgt_obj[src_prop_name] = tgt_prop

                case 'PROPERTIES_FLIP' :
                    if cible_name in newly_created_keys :
                        for tgt_prop_name in tgt_obj.keys() :
                            # if src_prop_name not in tgt_obj.keys(): #missing property
                                
                                tgt_prop = tgt_obj.get(tgt_prop_name)
                                if verbose:print('flipping property %s on %s'%(tgt_prop_name, tgt_obj.name))  
                                lat_index_cible = regex_get_lateral_index(cible_name, 
                                                                          regEx_pattern_flip=regEx_pattern_flip)                        
                                tgt_prop = conform_prop_data(tgt_prop,
                                                             flip=True,flip_re_pattern=regEx_pattern_flip,lat_index_cible=lat_index_cible,
                                                             off_v='>>>')                                                
                                tgt_obj[tgt_prop_name] = tgt_prop
                            
                case 'NON_NATIVE_DATA':
                    if src_obj.type == 'GPENCIL' :
                        for dir_key in non_native_gp_data :
                            if dir_key in dir(src_obj) :
                                if verbose:print(f"Copying {dir} on {src_obj.name}")
                                try :
                                    src_data = getattr(src_obj, dir_key)
                                    tgt_data = getattr(tgt_obj, dir_key)
                                    #make a local copy
                                    tgt_data = conform_prop_data(src_data,tgt_prop=tgt_data)
                                    #NO Need to set the data as it should be done in place by conform_prop_data
                                    # setattr(tgt_obj, dir_key, tgt_data)
                                
                                except Exception as e:
                                    print('WARNING! failed to copy %s on %s error: %s'%(dir_key, src_obj.name,e))
                                    pass    
                case 'NON_NATIVE_DATA_FLIP':
                    if cible_name in newly_created_keys :
                        for dir_key in non_native_gp_data :
                            if dir_key in dir(tgt_obj) and dir_key not in tgt_obj.keys() :
                                if verbose:print(f"Flipping {dir_key} on {tgt_obj.name}")
                                try :
                                    
                                    tgt_data = getattr(tgt_obj, dir_key)
                                    #make a local copy
                                    lat_index_cible = regex_get_lateral_index(cible_name, 
                                                                              regEx_pattern_flip=regEx_pattern_flip)
                                    tgt_data = conform_prop_data(tgt_data,
                                                                 flip=True,flip_re_pattern=regEx_pattern_flip,lat_index_cible=lat_index_cible,
                                                                 )
                                    #NO Need to set the data as it should be done in place by conform_prop_data
                                    # setattr(tgt_obj, dir_key, tgt_data)
                                
                                except Exception as e:
                                    print('WARNING! failed to copy %s on %s error: %s'%(dir_key, src_obj.name,e))
                                    pass          

                case 'LINK' :
                    parent_col = get_object_collections(src_obj, recursiv_dic = {}, get_elders = False)
                    for p in parent_col.keys() :
                        parent_col = bpy.data.collections[p]
                        #check if object is already linked
                        if tgt_obj.name not in [o.name for o in parent_col.objects] :
                            parent_col.objects.link(tgt_obj)
                case 'PARENT' :                    
                    if src_obj.parent is not None :
                        parent = bpy.data.objects[src_obj.parent.name]
                        tgt_obj.parent = parent
                        tgt_obj.matrix_parent_inverse = src_obj.matrix_parent_inverse
                case 'CONSTRAINTS' :
                    for src_constraint in src_obj.constraints :
                            #conform object constraints
                        if src_constraint.name not in [constraint.name for constraint in tgt_obj.constraints] : #missing constraint
                            tgt_obj.constraints.copy(src_constraint)
                        for constraint in tgt_obj.constraints :
                            if 'target' in dir(constraint) and constraint.target is not None :
                                parent = bpy.data.objects[constraint.target.name]
                                constraint.target = parent
                                if constraint.target.name_full.find('.blend') != -1 :
                                    print('WARNING! the constraint %s is targeting an external obj'%constraint.name)
                                    pass
                case 'BONES_CONSTRAINTS' :
                    if k in newly_created_keys and src_obj.type == 'ARMATURE' :
                        #go in pose mode
                        # if bpy.context.mode != 'POSE':
                        #     #select the tgt armature
                        #     # bpy.context.view_layer.objects.active = tgt_obj
                        #     # bpy.ops.object.mode_set(mode='POSE', toggle=False)
                        #     # bpy.ops.object.posemode_toggle()
                        #     pass
                        # object.pose.bones["2d1"].constraints["Locked Track"].target
                        for bone in src_obj.pose.bones :
                            #get tgt bones
                            tgt_bone = tgt_obj.pose.bones.get(bone.name)

                            for src_constraint in bone.constraints :
                                if src_constraint.name not in [constraint.name for constraint in tgt_bone.constraints] : #missing constraint
                                    tgt_bone.constraints.copy(src_constraint)
                                #check if targer dir in constraint

                                if 'target' in dir(src_constraint) and  src_constraint.target is not None :
                                    #get target constraint
                                    constraint = tgt_bone.constraints.get(src_constraint.name)
                                    parent = bpy.data.objects[src_constraint.target.name]
                                    constraint.target = parent
                                    if constraint.target.name_full.find('.blend') != -1 :
                                        print('WARNING! the constraint %s is targeting an external obj'%constraint.name)
                                        pass
                case 'MODIFIERS' :
                    for src_modifier in src_obj.modifiers :
                        if src_modifier.name not in [modifier.name for modifier in tgt_obj.modifiers] : #missing modifier
                            new_modifier = tgt_obj.modifiers.new(src_modifier.name, src_modifier.type)
                            for attr in dir(new_modifier):
                                try :
                                    setattr(new_modifier, attr, getattr(src_modifier, attr))
                                except :
                                    print('WARNING! the modifier %s has no attribute %s',src_modifier.name, attr)
                                    pass
                case 'GP_DATA':
                    if src_obj.type == 'GPENCIL' :
                        # sync_materials(sourceObject, selectedObject)
                        sync_materials(src_obj, tgt_obj)
                        sync_layers(src_obj.data, tgt_obj.data) #create missing layers
                        # sync_layers(sourceObject.data, selectedObject.data) #create missing layers
                case 'GP_MODIFIERS' :
                    for src_gpmodifier in src_obj.grease_pencil_modifiers :
                        if src_gpmodifier.name not in [gpmodifier.name for gpmodifier in tgt_obj.grease_pencil_modifiers] : #missing gpmodifier
                            new_gpmodifier = tgt_obj.grease_pencil_modifiers.new(src_gpmodifier.name, src_gpmodifier.type)
                            for attr in dir(new_gpmodifier):
                                try :
                                    setattr(new_gpmodifier, attr, getattr(src_gpmodifier, attr))
                                except :
                                    print('WARNING! the gp_modifier %s has no attribute %s',src_gpmodifier.name, attr)
                                    pass
                case 'SHADERS' :
                    for src_shadereffect in src_obj.shader_effects :
                        if src_shadereffect.name not in [shadereffect.name for shadereffect in tgt_obj.shader_effects] : #missing shadereffect
                            new_shadereffect = tgt_obj.shader_effects.new(src_shadereffect.name, src_shadereffect.type)
                            for attr in dir(new_shadereffect):
                                try :
                                    setattr(new_shadereffect, attr, getattr(src_shadereffect, attr))
                                except :
                                    print('WARNING! the gp_modifier %s has no attribute %s',src_shadereffect.name, attr)
                                    pass
                case 'ACTIONS' :                    
                    if k in newly_created_keys :
                        if verbose:print(f"Copying actions on {src_obj.data.name}")
                        if src_obj.animation_data and src_obj.animation_data.action is not None:  
                            if tgt_obj.animation_data:
                                #add an empty action to the target object
                                tgt_obj.animation_data_create() 
                               
                                # newaction = src_obj.animation_data.action.copy()
                                name = src_obj.animation_data.action.name
                                try:
                                    for act in bpy.data.actions :
                                        if act is not None and act.name == src_obj.animation_data.action.name :
                                            bpy.data.actions.remove(act)
                                            break
                                except Exception as e:
                                    pass
                                # newaction.name = name
                                # tgt_obj.animation_data.action = newaction
                                tgt_obj.animation_data.action = bpy.data.actions.new(name = src_obj.animation_data.action.name)
                                if verbose: print('new action name : %s'%name)

                                #CREATE THE FCURVES
                                if verbose:print(f"Creating fcurves on {tgt_obj.animation_data.action.name}")
                                for src_fcurve in src_obj.animation_data.action.fcurves :
                                    if fc_is_from_array(src_fcurve) :
                                        array_index = src_fcurve.array_index
                                    else : 
                                        array_index = -1
                                    tgt_obj.animation_data.action.fcurves.new(src_fcurve.data_path, 
                                                                              index=array_index)
                                
                                # search for oprhan action with the same name                              
                                
                            else:
                                print('WARNING!!! the target %s object has no animation data to copy %s action '%(tgt_obj.name, src_obj.animation_data.action.name))
                case 'OBJ_DRIVERS' :
                    if src_obj.animation_data:
                        if verbose:print("Gathering %s drivers on %s"%(len(src_obj.animation_data.drivers),src_obj.name))
                        
                        #make a list with each src_obj.animation_data.drivers
                        drivers = [driver for driver in src_obj.animation_data.drivers]
                        
                        #for each driver in the source object                        
                        for src_driver in drivers :
                            if verbose:print('Copying driver %s on %s'%(src_driver.data_path, tgt_obj.name))
                            #check if there is an identical datapath
                            if tgt_obj.animation_data:
                                if  (src_driver.data_path in [driver.data_path for driver in tgt_obj.animation_data.drivers] 
                                    and  k not in newly_created_keys):
                                        if verbose:print('The driver %s is already present in the target object'%src_driver.data_path)
                                else:
                                    copy_driver(src_driver, tgt_obj)
                            else:

                                copy_driver(src_driver, tgt_obj)                            
                    else:
                        print('WARNING! the source %s object has no animation data'%(src_obj.name))
                case 'GP_DRIVERS':
                    #add  drivers in the greasepencil data
                    if src_obj.data and src_obj.data.animation_data :
                        if verbose:print("Gathering %s GP_drivers on %s"%(len(src_obj.data.animation_data.drivers),src_obj.data.name))
                        drivers = [driver for driver in src_obj.data.animation_data.drivers]

                        #for each driver in the source object                        
                        for src_driver in drivers :
                            if verbose:print('Copying driver %s N° %s on %s'%(src_driver.data_path,src_driver.array_index, tgt_obj.name))
                            #check if there is an identical datapath
                            tgt_driver = get_driver(tgt_obj.data, src_driver.data_path, src_driver.array_index)
                            if  (tgt_driver is not None ):
                                        if verbose:print('The driver is already present in the target object')
                                        if  k in newly_created_keys:
                                            if verbose:print('Relinking...')                                       
                                            # tgt_obj.data.animation_data.drivers.remove(tgt_driver)
                                            copy_driver(src_driver, tgt_obj.data,tgt_driver=tgt_driver)                                
                            else:
                                if verbose:print('Building driver...')    
                                copy_driver(src_driver, tgt_obj.data)      

                        
                case 'DUPLICATE_FLIP':
                    if not tgt_obj :
                        lat_index = regex_get_lateral_index(src_obj.name, 
                                                           regEx_pattern_flip=regEx_pattern_flip)
                        if lat_index is not None :
                            new_name = regex_flip_name(src_obj.name, 
                                                    regEx_pattern_flip=regEx_pattern_flip, 
                                                    center_suffix=center_suffix)
                            if lat_index != 0: #lateral identical object
                                #copy object                        
                                new_ob = duplicate_object(src_obj)                            
                                new_ob.name = new_name
                                if cible_name != new_ob.name :                                    
                                    print('WARNING! the object %s has been named %s:RETRY'%(cible_name, new_ob.name))
                                    # fantom_obj = bpy.data.objects.get(cible_name)
                                    # bpy.data.objects.remove(fantom_obj)
                                    new_ob.name = new_name
                                    print('new name : %s'%new_ob.name)
                                # f_ob.data = ob.data.copy()
                                new_ob.data.name = new_ob.name
                                if new_ob.animation_data and new_ob.animation_data.action :
                                    new_ob.animation_data.action.name = new_ob.name + '_Action' #TODO remove the hardcoded suffix
                            elif lat_index == 0: #center object >> empty obj
                                if 'EMPTY' not in bpy.data.grease_pencils.keys() :
                                    emptyGP = bpy.data.grease_pencils.new('EMPTY')
                                else : emptyGP = bpy.data.grease_pencils['EMPTY']
                                new_ob = bpy.data.objects.new(new_name, emptyGP)

                            tgt_objects_dic[cible_name] = new_ob
                            
                            newly_created_keys.append(new_ob.name)
                        else:
                            print('WARNING! the object %s return no lateral index'%src_obj.name)
                case 'LINK_FLIP':
                    if cible_name in newly_created_keys :
                        #Get source parent col
                        parent_col = get_object_collections(src_obj, recursiv_dic = {}, get_elders = False)
                        
                        for p in parent_col :
                            #Reverse name if lateralized
                            col_cible_name = regex_flip_name(p, 
                                                        regEx_pattern_flip=regEx_pattern_flip
                                                        )
                            #Get target parent col
                            col_cible = bpy.data.collections.get(col_cible_name)
                            # link obj to col
                            col_cible.objects.link(tgt_obj)
                case 'CONSTRAINTS_FLIP':
                    #FLIPPER
                    if re.search(regEx_pattern_flip.get(0),k):
                        if cible_name in newly_created_keys:
                            # if 'EMPTY' not in bpy.data.grease_pencils.keys() :
                            #                     emptyGP = bpy.data.grease_pencils.new('EMPTY')
                            # else : emptyGP = bpy.data.grease_pencils['EMPTY']

                            # flipper = bpy.data.objects.new(root_parent.name + self.center_suffix, emptyGP)
                            # context.scene.collection.objects.link(flipper) 
                            # flipper.select_set(True)
                            tgt_obj.scale[0] = -1
                            
                            child_of = tgt_obj.constraints.new('CHILD_OF')
                            child_of.target = src_obj
                            child_of.inverse_matrix = Matrix()
                    #FLIPPED
                    elif regex_get_lateral_index(cible_name,regEx_pattern_flip=regEx_pattern_flip) != 0: #if later obj
                        if not tgt_obj :
                            print('WARNING! the object %s has no target object'%src_obj.name)
                            
                        else:
                            for c in tgt_obj.constraints :
                                #check if c as a dir target
                                if 'target' in dir(c) and c.target is not None :
                                    # LATERAL CONSTRAINT
                                    new_tgt = None
                                    lat_ind = regex_get_lateral_index(
                                                                        c.target.name,
                                                                        regEx_pattern_flip=regEx_pattern_flip
                                                                        )
                                    if  lat_ind != 0 : 
                                        new_name = regex_flip_name(c.target.name, 
                                                                regEx_pattern_flip=regEx_pattern_flip)
                                        new_tgt = bpy.data.objects.get(new_name)
                                    #FLIPPER CONSTRAINT
                                    elif c.type == 'CHILD_OF'and lat_ind == 0  :
                                        
                                        flipper_name = regex_flip_name(c.target.name, 
                                                                regEx_pattern_flip=regEx_pattern_flip,
                                                                center_suffix=center_suffix)
                                        new_tgt = bpy.data.objects.get(flipper_name)         
                                    if new_tgt :
                                        if verbose : print('setting target for %s : %s on object %s'%(c.target.name, new_tgt.name, tgt_obj.name))
                                        c.target = new_tgt       
                                    

                # case 'PROPERTIES_FLIP':
                #     if cible_name in newly_created_keys :
                #         if verbose:print(f"Flipping properties on {tgt_obj.name}")
                #         for key in tgt_obj.keys() :
                #             prop = tgt_obj.get(key)
                #             def property_flip( prop):
                #                 type_prop = type(prop)
                                
                #                 if isinstance(prop, (int, float, str)):
                #                     return prop
                #                 elif type_prop == list :
                #                     prop_list = []
                #                     for o in prop :
                #                         prop_list.append(property_flip(o))
                #                     return prop_list                                    
                #                 elif type_prop == bpy.types.Object :
                #                     #get prop lateral index
                #                     lat_index = regex_get_lateral_index(prop.name, 
                #                                                         regEx_pattern_flip=regEx_pattern_flip)
                #                     if lat_index is not None and lat_index!=0:
                #                         if verbose:print(f"Pointer to object {prop.name} is lateralized")
                #                         cible_lat_index = regex_get_lateral_index(tgt_obj.name, 
                #                                                         regEx_pattern_flip=regEx_pattern_flip)
                #                         if lat_index!= cible_lat_index:
                #                             new_name = regex_flip_name(prop.name, 
                #                                                         regEx_pattern_flip=regEx_pattern_flip)                                                                 
                                                                    
                #                             prop = bpy.data.objects.get(new_name)
                #                 elif prop is None :
                #                     return prop
                #                 else:
                #                     if verbose:print(f"-property {key} type isn't catch: {type(prop)} \n --may be IDPropertyGroup or IDPropertyArray")
                #                     try:
                #                         if hasattr(prop, 'to_dict'):   
                #                             if verbose:print(f"--property is IDPropertyGroup")                                 
                #                             for k in prop.keys() :
                #                                 prop[k] = property_flip(prop.get(k))
                #                         elif hasattr(prop, 'to_list'):
                #                             if verbose:print(f"--property is IDPropertyArray")     
                #                             for i in range(len(prop)) :
                #                                 prop[i] = property_flip(prop[i])
                #                         else:
                #                             if verbose : '--property is not IDPropertyGroup or IDPropertyArray...cannot be treated'

                #                     except Exception as e:
                #                         if verbose:print(f"--property cannot be treated as dic. Try to treat it as array")

                #                 return prop
                #             tgt_obj[key]= property_flip(prop)
                case 'OBJ_DRIVERS_FLIP' :

                    if cible_name in newly_created_keys and src_obj.animation_data:
                        if verbose:print("Gathering %s drivers on %s"%(len(src_obj.animation_data.drivers),src_obj.name))
                        
                        #make a list with each src_obj.animation_data.drivers
                        drivers = [driver for driver in src_obj.animation_data.drivers]
                        
                        #for each driver in the source object                        
                        for src_drivers_fc in drivers :
                            if verbose:print('Copying driver %s on %s'%(src_drivers_fc.data_path, tgt_obj.name))
                            #check if there is an identical datapath
                            if (tgt_obj.animation_data
                                and (src_drivers_fc.data_path in [driver.data_path for driver in tgt_obj.animation_data.drivers])
                                ):
                                if verbose:print('The driver %s is already present in the target object'%src_drivers_fc.data_path)
                                array_index = src_drivers_fc.array_index
                                
                                if array_index is None or array_index == -1:
                                    array_index = 0
                                tgt_fc = get_driver(tgt_obj, src_drivers_fc.data_path, index = array_index)
                            
                                if tgt_fc is None :
                                    if verbose:print('WARNING! Couldnt find driver %s'%src_drivers_fc.data_path)
                                    continue 
                                 
                                src_driver = src_drivers_fc.driver                             
                                
                                if isinstance(tgt_fc, list):
                                    driver_list = tgt_fc
                                else:
                                    driver_list = [tgt_fc]
                                for driver in driver_list:  
                                    
                                    for src_var in src_driver.variables:
                                        tgt_drv = driver.driver
                                        for var in tgt_drv.variables:
                                            if var.name == src_var.name:
                                                tgt_var = var
                                                break

                                        if not tgt_var:
                                            if verbose:print("couldn't find variable %s"%(src_var.name) )                                            
                                        
                                        try:
                                            #v2.targets[0].data_path = tgt_ob.data.data_path
                                            for i in range(len(src_var.targets)):  
                                                src_target = src_var.targets[i]  
                                                tgt_target = tgt_var.targets[i]        
                                                src_path = src_target.data_path
                                                src_name = src_target.id.name
                                                #get lat index
                                                lat_index = regex_get_lateral_index(src_name, 
                                                                                        regEx_pattern_flip=regEx_pattern_flip)
                                                if lat_index is  None or lat_index==0:
                                                    continue

                                                if verbose:print(f"Pointer to object {src_name} is lateralized")
                                                
                                                
                                                tgt_path = regex_flip_name(src_path, 
                                                                                    regEx_pattern_flip=regEx_pattern_flip)                                                               
                                                tgt_name = regex_flip_name(src_name, 
                                                                                    regEx_pattern_flip=regEx_pattern_flip)        
                                                tgt_target.data_path = tgt_path                                                
                                                #get the object in tgt_ob's scene
                                                tgtobj = bpy.data.objects.get(tgt_name)
                                                tgt_target.id = tgtobj
                                        except:
                                            if verbose:print("Driver target flip failed on %s"%(src_var.data_path) )
                       
                    else:
                        print('WARNING! the source %s object has no animation data'%(src_obj.name))
                case 'BONES_FLIP':
                    if cible_name in newly_created_keys and src_obj.type == 'ARMATURE' :                        
                        for src_bone in src_obj.pose.bones :
                            #get tgt bones
                            tgt_bone = tgt_obj.pose.bones.get(src_bone.name)

                            for src_constraint in src_bone.constraints :
                                if src_constraint.name not in [constraint.name for constraint in tgt_bone.constraints] : #missing constraint
                                    tgt_bone.constraints.copy(src_constraint)
                                #check if targer dir in constraint

                                if 'target' in dir(src_constraint) and  src_constraint.target is not None :
                                    #get target constraint
                                    constraint = tgt_bone.constraints.get(src_constraint.name)
                                    fliped_parent_name = regex_flip_name(src_constraint.target.name, 
                                                                        regEx_pattern_flip=regEx_pattern_flip)
                                    parent = bpy.data.objects[fliped_parent_name]
                                    constraint.target = parent
                                    if verbose:print(f"Flipping constraint  {constraint.name} target on {tgt_obj.name}")
                                    if constraint.target.name_full.find('.blend') != -1 :
                                        print('WARNING! the constraint %s is targeting an external obj'%constraint.name)
                                        pass
                case 'LATERAL_PROPERTIES_FLIP' :   
                    if cible_name in newly_created_keys :
                        for const in lat_properties:
                            if src_obj.get(const) is not None :
                                #inverse binary value
                                tgt_obj[const] = not src_obj[const]
            
                case _:
                    print('WARNING! the action %s is not understood'%a)

    return newly_created_keys


def conform_armature(src_arm,tgt_arm, verbose=True, actions = [],**kwargs):   
    '''conform an armature to another
    '''
    #create tgt_arm
    # bpy.ops.armature_add()
    # tgt_arm = bpy.context.view_layer.objects.active
    # tgt_arm = bpy.data.armatures.new(src_arm.name)
    # #set activ obj tgt arm
    #update view layer
    bpy.context.view_layer.update()
    bpy.context.view_layer.objects.active = tgt_arm
    bpy.ops.object.mode_set(mode='EDIT', toggle=False)
    
    tgt_edit_bones = tgt_arm.edit_bones
    #CLear bone of tgt
    # tgt_edit_bones.clear()
    #copy bones

    for src_bone in src_arm.bones :
        new_bone = tgt_edit_bones.new(src_bone.name)
        #copy bone properties
        conform_bone(src_bone, new_bone)
        

def conform_bone(src_bone,tgt_bone,attributes=[ 'bbone_curveinx',
                                                'bbone_curveinz',
                                                'bbone_curveoutx',
                                               ]):
    for a in attributes :
        match a :

            case _:
                try:
                    setattr(tgt_bone, a, getattr(src_bone, a))
                except:
                    print("Confo keyframe failed on %s, attribute %s"%(tgt_bone,a) )       
# ------------------- SYNCHRONISER -------------------
def conform_prop_data( source_prop,tgt_prop=None,
                      flip = False,flip_re_pattern = None,lat_index_cible = None,
                      verbose=True,off_v = '' ) :
    source_prop_type = type(source_prop)
    if verbose:print(off_v+'copying source prop of type : %s'%source_prop_type)
    read_only = False
    #if type is <class 'list'>
    if source_prop_type == list :
        #check if it's a list of objects
        local_list = []
        if len(source_prop) > 0  :
            #get local version of the list                                        
            for o in source_prop :                                            
                local_list.append(conform_prop_data(o,
                                                    flip=flip, flip_re_pattern=flip_re_pattern, lat_index_cible=lat_index_cible,
                                                    verbose = verbose,off_v = off_v+'-'))
                
        tgt_prop = local_list
    elif source_prop_type == bpy.types.Object :
        if flip:        
            #get lat index
            lat_index = regex_get_lateral_index(source_prop.name, 
                                                regEx_pattern_flip=flip_re_pattern)   
            if lat_index!=lat_index_cible: 
                cible_name = regex_flip_name(source_prop.name, flip_re_pattern)
                if verbose:print(off_v+'flipping %s to %s'%(source_prop.name, cible_name))
            else:
                if verbose:print(off_v+'No need to flip %s'%(source_prop.name))
                cible_name = source_prop.name
        else:
            cible_name = source_prop.name
            if verbose:print(off_v+'setting %s to local %s'%(source_prop.name, cible_name))
        tgt_prop = bpy.data.objects.get(cible_name)
    #if it  <class 'IDPropertyGroup'> 
    
    elif hasattr(source_prop_type, 'to_dict'): 
        if tgt_prop is None :
            tgt_prop = source_prop 
        #iterate trough properties and copy them
        for k,sub_src_prop in source_prop.items() :
            tgt_prop[k] = conform_prop_data(sub_src_prop,tgt_prop = tgt_prop.get(k),
                                            flip=flip, flip_re_pattern=flip_re_pattern, lat_index_cible=lat_index_cible,
                                            verbose = verbose,off_v = off_v+'-')
    elif hasattr(source_prop_type, 'to_list'): 
        if tgt_prop is None :
            tgt_prop = source_prop        
        #iterate trough properties and copy them
        for i,sub_src_prop in enumerate(source_prop) :
            tgt_prop[i] = conform_prop_data(sub_src_prop,tgt_prop=tgt_prop[i],                                            
                                            flip=flip, flip_re_pattern=flip_re_pattern, lat_index_cible=lat_index_cible,
                                            verbose = verbose,off_v = off_v+'-')
    elif hasattr(source_prop_type, 'clear'):
        #CollectionProperty READONLY!        
        i=0
        for v in source_prop.values() :
            print('%s copying %s'%(off_v,v))              
            src_value = source_prop[i] 
            tgt_value = tgt_prop[i]
            tgt_value =conform_prop_data(src_value,tgt_prop=tgt_value,                                         
                                         flip=flip, flip_re_pattern=flip_re_pattern, lat_index_cible=lat_index_cible,
                                         verbose = verbose,off_v = off_v+'-')
            # tgt_prop[i] = tgt_value
            i+=1 
        
        pass    
    elif 'bl_idname' in dir(source_prop) :
        for dir_key in dir(source_prop) :
            if dir_key.startswith('__') or dir_key.startswith('bl_') or dir_key.startswith('rna_') :
                continue
            if verbose:print(f"{off_v} Copying {dir_key} of {source_prop.bl_idname}")    

            src_data = getattr(source_prop, dir_key)
            tgt_data = getattr(tgt_prop, dir_key)
            local_tgt_data = conform_prop_data(src_data,tgt_prop=tgt_data,
                                               flip=flip, flip_re_pattern=flip_re_pattern, lat_index_cible=lat_index_cible,
                                               verbose = verbose,off_v = off_v+'-')

            try :
                setattr(tgt_prop, dir_key, local_tgt_data)
            except Exception as e:
                print('WARNING! failed to copy %s on %s error: %s'%(dir_key, source_prop.bl_idname,e))
                pass        
    else:                                    
        tgt_prop = source_prop 
    return tgt_prop
                            

def duplicate_object(obj, data=True, actions=True):
    '''creates a copy of a src object in the work scene
        args : obj = object to copy
        data : copy data. If not data are instance of source object
        actions : copy actions. If not actions are instance of source object   
        returns : the new object
        '''
    obj_copy = obj.copy()
    if data and obj_copy.data:
        obj_copy.data = obj.data.copy()
        
    if actions and obj_copy.animation_data and obj_copy.animation_data.action:
        obj_copy.animation_data.action = obj.animation_data.action.copy()
    return obj_copy

def import_object(src_ob, collections):
    '''creates a copy of a src object in the work scene
        args : src_ob = object to copy
        collections : list of collections where to link the new object
        returns : the new object'''
    new_ob = src_ob.copy()
    new_ob.name = src_ob.name

    if src_ob.data is not None :
        new_ob.data = src_ob.data.copy()
        new_ob.data.name = src_ob.data.name
    new_ob.empty_display_size = src_ob.empty_display_size
    new_ob.empty_display_type = src_ob.empty_display_type
    for collection in collections :
        collection.objects.link(new_ob)

    #clear template timeline
    if new_ob.animation_data :
        new_ob.animation_data_clear()
    if new_ob.type == 'GPENCIL':
        for layer in new_ob.data.layers :
            for frame in reversed(layer.frames) :
                layer.frames.remove(frame)
    #reload material list with local/library materials
    new_ob.data.materials.clear()
    sync_materials(src_ob, new_ob)

    #reassign parentship
    src_parent = new_ob.parent
    if src_parent is not None : 
        parent = bpy.data.objects[src_parent.name]
        new_ob.parent = parent
        new_ob.matrix_parent_inverse = src_ob.matrix_parent_inverse
        
    for constraint in new_ob.constraints : 
        #TODO: some constraint still keeping source object as target.
        # It may be because some objects arent created in the current scene ATM 
        #solution would be progressive creation of objects
        if 'target' in dir(constraint) and constraint.target is not None :
            parent = bpy.data.objects[constraint.target.name]
            constraint.target = parent
            #inverse matrix already set by copying the object
            if constraint.target.name_full.find('.blend') != -1 :
                pass

    return new_ob

def sync_collections(src_collection, tgt_collection) :
    '''syncs attribute between two collections'''
    tgt_collection.hide_select = src_collection.hide_select
    tgt_collection.hide_viewport = src_collection.hide_viewport
    tgt_collection.hide_render = src_collection.hide_render
    tgt_collection.color_tag = src_collection.color_tag
    #copy exclude from view layer     
    # for view_layer in bpy.context.scene.view_layers :
    #     for l in view_layer.layer_collection.children:
    #         l.exclude = l.name.lower() != vl.name.lower()
        
    
def sync_palettes(src_scene, tgt_scene) :
    #Get current gpmatpalettes
    try:
        src_palettes = src_scene.gpmatpalettes
        tgt_palettes = tgt_scene.gpmatpalettes
        tgt_path = [p.source_path for p in tgt_palettes.palettes]
        src_paths = []
        for p in src_palettes.palettes:
            if (p.source_path not in src_paths 
                and p.source_path not in tgt_path) :                
                src_paths.append(p.source_path) 
        for p in src_paths :            
            bpy.ops.scene.import_palette('EXEC_DEFAULT',filepath=p)

    except Exception as e:
        print('WARNING! palettes sync failed: ',e)
    pass


# ------------------- GETTERS -------------------

def get_object_collections(src_object, recursiv_dic = {}, get_elders = True) :
    '''returns a dictionnary of all the collections where the object is linked
    args : context = the context of the blend file where the object is imported
              src_object = the object you want to get collections from
                recursiv_dic = the dictionnary where you want to store collections
                get_elders = if True, returns a dictionnary of all the collections ancestors of a collection
    '''
    # get a dictionnary of all the collections where the object is linked
    for src_col in src_object.users_collection :
        if src_col.name not in recursiv_dic.keys() :
            recursiv_dic[src_col.name] = src_col
            if get_elders:
                recursiv_dic = recursiv_dic | get_collections_elders(src_col,recursive_dic=recursiv_dic)

    return recursiv_dic

def get_collections_elders(collection, recursive_dic={}) :
    '''returns a dictionnary of all the collections ancestors of a collection
    args : collection = the collection you want to get ancestors from
           recursive_dic = the dictionnary where you want to store ancestors'''
    parents = get_collection_parents(collection)
    for parent_collection in parents :
        if parent_collection.name not in recursive_dic.keys() :
            recursive_dic[parent_collection.name] = parent_collection
            recursive_dic = recursive_dic| get_collections_elders(parent_collection, recursive_dic)
    return recursive_dic

def get_collection_parents(collection,collections_list = None):
    if not collections_list :
        collections_list =  bpy.data.collections
    parents = []
    for parent_collection in collections_list :
        for child in parent_collection.children :
            if collection == child :
                parents.append(parent_collection)
    return parents

def get_objects_childs_dic(objects, objects_list,
                           constraint_list = [
                                            'CHILD_OF',
                                            'LIMIT_DISTANCE',
                                            'TRACK_TO',
                                            'COPY_LOCATION',
                                            'COPY_ROTATION'
                                              ],#USED in rig -limit distance-track to-child of-copy loc-copy rot
                           recursive_dic = {},
                           add_self = True,
                           descendant_search=True):
    """returns a dictionnary of all the objects in the dependancy tree (childrens, 'child of' constraints targeting the object)
    args : objects = the objects you wan't to get childrens from
           objects_list = the list where you want to look for children (basically bpy.context.scene)
           constraint_list = the list of constraints type that should be looked as parenting
           recursive_dic = the dictionnary where you want to store objects, and look if it as already be found
           add_self = if True, add the object itself to the dictionnary        

    """
    for ob in objects :
        if add_self and ob.name not in recursive_dic.keys() :
            recursive_dic[ob.name] = ob
        #get natural children
        for child in ob.children_recursive :
            if child.name not in recursive_dic.keys() :
                recursive_dic[child.name] = child
                if descendant_search :
                    recursive_dic |= get_objects_childs_dic(
                        [child], objects_list, constraint_list, recursive_dic, descendant_search)

        #search for constraint children        
        for potential_child in objects_list :
            if potential_child.name not in recursive_dic.keys() :
                if len([constraint for constraint in potential_child.constraints 
                                        if constraint.type in constraint_list 
                                        and  constraint.target is not None
                                        and constraint.target.name == ob.name]) > 0 :
                    recursive_dic[potential_child.name] = potential_child
                    if descendant_search :
                        recursive_dic |= get_objects_childs_dic(
                            [potential_child], objects_list, constraint_list, recursive_dic, descendant_search)
                    
    return recursive_dic


# ------------------- OLD GET AND CREATE -------------------

def get_objects_and_childs(objects, objects_list):
    '''returns a list of all the objects in the dependancy tree (childrens, 'child of' constraints targeting the object)
    args : objects = the objects you wan't to get childrens from
           objects_list = the list where you want to look for children (basically bpy.context.scene)'''
    result = []
    for ob in objects :
        if ob not in result :
            result.append(ob)
        for child in get_constraint_children_recursive(ob, objects_list, []) :
            if child not in result :
                result.append(child)
    return result

def get_constraint_children_recursive(object, objects_list, ctrl_list=[]):
    '''
    returns a list of all the objects in the dependancy tree (childrens, 'child of' constraints targeting the object) recursively
    args : object = the object you wan't to get childrens from
           objects_list = the list where you want to look for children (basically bpy.context.scene)
           ctrl_list = provide an empty list, this is to avoid infinite recursions
    '''
    childs = [] 
    for child in object.children_recursive :
        if child not in childs :
            childs.append(child)  
            
    for ob in objects_list :
        if ob not in childs and len([constraint for constraint in ob.constraints 
                                     if constraint.type == 'CHILD_OF' 
                                     and constraint.target is not None
                                     and constraint.target == object]) > 0 :
            childs.append(ob)
                
    for child in childs :
        if child not in ctrl_list :
            ctrl_list.append(child)
            for granchild in get_constraint_children_recursive(child, objects_list, ctrl_list) :
                if granchild not in childs :
                    childs.append(granchild)
    return childs

def create_missing_collection_recursive(context, src_col) :
    '''creates a collection and its parents if they are missing
           args : context = bpy.context
           src_col = the collection to create           
           returns : the created collection and a list of the recursively created collections names'''
    created_collections_names = []
    print('creating missing collection :', src_col.name)
    collection = bpy.data.collections.new(src_col.name)

    

    sync_collections(src_col, collection)

    # TOCHECK : 
    created_collections_names.append(src_col.name)

    for src_parent_col in get_collection_parents(src_col) :
        parent_col_list = [coll for coll in context.scene.collection.children_recursive if coll.name == src_parent_col.name]
        if len(parent_col_list) == 1 :
            parent_col = parent_col_list[0]
        else : #parent collection is also missing
            parent_col, rec_cre_col_names = create_missing_collection_recursive(context, src_parent_col)  
            for col_name in rec_cre_col_names :
                if col_name not in created_collections_names :
                    created_collections_names.append(col_name)     
        parent_col.children.link(collection)

        
    return collection, created_collections_names

def get_and_create_object_collections(context, src_object, created_collections_names) : 
    #determine if collections exists
    collections = []
    for src_col in src_object.users_collection :
        if src_col.name not in [col.name for col in context.scene.collection.children_recursive] and src_col.name not in created_collections_names : #missing collection
            collection, created_col_names = create_missing_collection_recursive(context, src_col)
            collections.append(collection)
            for col_name in created_col_names :
                if col_name not in created_collections_names :
                    created_collections_names.append(col_name)

        else : #collection exists
            for col in context.scene.collection.children_recursive :
                if col.name == src_col.name :
                    collections.append(col)
                    break
    return collections, created_collections_names

def create_object_and_collection(context, src_object, created_collections_names) :
            #determine if collections exists
            collections, created_collections_names = get_and_create_object_collections(context, src_object, created_collections_names)

            new_ob = import_object(src_object, collections)
            create_anim_data(new_ob)
            # add newly created collections
            return created_collections_names


#DICTIONARY TOOLS

def get_from_dic_or_create(dic, key, default_value=None, create = True) :
    '''returns the value of a key in a dictionary, if the key doesn't exist, it creates it with the default value'''
    if key in dic.keys() :
        return dic[key]
    else :
        if create :
            dic[key] = default_value
            return dic[key]
        return default_value
    
    #CHECKER
def is_object_indirect(obj):
    '''returns True if the object is indirect (linked from another scene)'''
    if obj.library is not None :
        return True
    elif obj.data.is_indirect:
        return True
    return False

# for object in bpy.context.scene.objects :
#     if is_object_indirect(object) :
#         print(object.name, 'is indirect')
#     else :
#         print(object.name, 'is not indirect')