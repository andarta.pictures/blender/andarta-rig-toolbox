import bpy 
import subprocess
import sys
import os
import importlib.util



#get bender user directory
install_path = bpy.utils.user_resource("SCRIPTS")

def ensure_lib(module_name,install_name = None,verbose = True):
    '''Check if a module is installed, if not, install it
    Return True if the module is accessible, False otherwise'''
    if install_name is None:
        install_name = module_name    
    
    if check_lib(module_name):
        return True

    python_path = sys.executable
    subp = subprocess.run([python_path, "-m", "ensurepip"])
    print(subp.returncode) 
    if subp.returncode != 0:
        return False

    subp = subprocess.run([python_path, "-m", "pip", "install",
                            "--target=" + os.path.join(install_path,'modules'),
                            install_name, "--upgrade"])
    if subp.returncode != 0:
        return False
    if not check_lib(module_name):
        print('Installation done but cannot import module, try to restart blender')
        return False
    return True

def check_lib(module_name):
    spam_spec = importlib.util.find_spec(module_name)
    found = spam_spec is not None
    if found:
        print('module %s found'%module_name)
        return True
    else:
        print('module %s not found'%module_name)
        return False


def import_libs(libs=[('numpy','*'),('matplotlib.pyplot','*')],force_install = False):
    print("Importing libraries...")
    for name,def_names in libs:
        print("Importing " + name + "...")
        if not import_lib(name,def_names,install=force_install):
            print("Error: Could not import nor install" + name)
            return False
    return True

def import_lib(lib_name,def_names = '*',verbose = True,install = False):
    '''Try to import a library based on its name
    If it fails, try to install it
    If it fails, print error message and return false'''
    try:
        exec('from '+lib_name+' import '+def_names)
        return True
    except ImportError:
        try:            
            #extract base libname if it is a subpackage
            if install:
                if '.' in lib_name:
                    lib_name = lib_name.split('.')[0]
                if verbose: print("Installing " + lib_name + "...")
                import pip
                pip.main(['install ', lib_name]) #deprecated
                #To avoid this problem you can invoke Python with '-m pip' instead of running pip directly.               
                exec('from '+lib_name+' import '+def_names)
                return True
            else:
                print("Error: Could not import " + lib_name)
                return False
        except:
            print("Error: Could not import " + lib_name)
            return False
