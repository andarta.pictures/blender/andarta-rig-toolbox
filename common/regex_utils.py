import re
# TOOLS
def regex_get_lateral_index(baseName, regEx_pattern_flip = {
                                -1 : '_L$',
                                0 : '_C$',
                                1 : '_R$', 
                                'holder' : 'HOLDER_[L,R]$'
                            }):                     
    '''returns the lateral index of an object based on a regex pattern
    args : baseName = the name of the object you want to get the lateral index from
              regEx_pattern_flip = the regex pattern used to get the lateral index
    return : the lateral index
    '''
    lateral_ind =None
    for key in regEx_pattern_flip.keys() :
        if re.search(regEx_pattern_flip[key], baseName) :
            lateral_ind = key
            break
    return lateral_ind

def regex_flip_name(baseName, 
                    regEx_pattern_flip = {
                                -1 : '_L$',
                                0 : '_C$',
                                1 : '_R$', 
                                'holder' : 'HOLDER_[L,R]$'
                            },
                    center_suffix = ''
                    ):
    '''returns the flipped name of an object based on a regex pattern
    args : baseName = the name of the object you want to flip
           regEx_pattern_flip = the regex pattern used to flip the name
    return : the flipped name
    '''
    #get the flip value
    flip_value = regex_get_lateral_index(baseName, regEx_pattern_flip)
    #flip the name
    if flip_value == None :
        return baseName
    elif flip_value == 0 :
        return baseName+center_suffix
    else :
        return re.sub(
            regEx_pattern_flip[flip_value], 
            regEx_pattern_flip[-flip_value].replace('$',''),
            baseName)


