import re
import bpy
from .regex_utils import regex_flip_name, regex_get_lateral_index
from .gp_utils import  Pseudo_gp_frame,Pseudo_stroke, colormatch,frame_confo, get_now_gpframe, move_gp_frame, point_confo, stroke_confo
from mathutils import Vector, Matrix, Euler
import numpy as np

def assign_matrix_basis(ob, matrix, frame_nb):
    loc, rot, sca = matrix.decompose()
    #print('assign to', ob.name, loc, rot.to_euler(), sca)

    ob.location = loc
    ob.rotation_euler = rot.to_euler()
    ob.scale = sca

    a= ob.keyframe_insert(data_path ='location', index = -1, frame = frame_nb)
    b= ob.keyframe_insert(data_path ='rotation_euler', index = -1, frame = frame_nb)
    c= ob.keyframe_insert(data_path ='scale', index = -1, frame = frame_nb)  
    #print(a,b,c) 

def snap_parented_object(ob, target_matrix_world):
    '''assigns and keyframes transformation matrix to a parented object'''
    bpy.context.view_layer.update()
    parent = None
    for c in ob.constraints :
        if c.type == 'CHILD_OF' and c.influence == 1 and c.target is not None:
            if c.target.type == 'ARMATURE' and c.subtarget != '' :
                parent = c.target.pose.bones[c.subtarget]
                inv_parent = Matrix(c.target.matrix_world @ parent.matrix)
            else :
                parent = c.target
                inv_parent = Matrix(parent.matrix_world)
            break
    if parent is None :
        inv_parent = Matrix()  
    else :
        inv_parent.invert()  
    result = Matrix(inv_parent @ target_matrix_world)
    assign_matrix_basis(ob, result, bpy.context.scene.frame_current)
    return

class pseudo_keyframe:
    """store keyframe data without it being a blender keyframe"""
    amplitude = None 
    back = None 
    co = None 
    co_ui = None 
    easing = None 
    handle_left = None 
    handle_left_type = None 
    handle_right = None 
    handle_right_type = None 
    interpolation = None 
    period = None 
    select_control_point = None 
    select_left_handle = None
    select_right_handle = None
    type = None
    def __init__(self, src_kf):
        confo_keyframe(src_kf,self, attributes = ['amplitude', 
                                       'back', 
                                       'co', 
                                       'co_ui', 
                                       'easing', 
                                       'handle_left', 
                                       'handle_left_type', 
                                       'handle_right', 
                                       'handle_right_type', 
                                       'interpolation', 
                                       'period', 
                                       'select_control_point', 
                                       'select_left_handle',
                                       'select_right_handle',
                                       'type'])

    def __repr__(self):
        return 'pseudo_keyframe(%s, %s, %s, %s, %s)'%(self.value, self.frame, self.interpolation, self.handle_left, self.handle_right)

    def __str__(self):
        return 'pseudo_keyframe(%s, %s, %s, %s, %s)'%(self.value, self.frame, self.interpolation, self.handle_left, self.handle_right)

    def __eq__(self, other):
        if isinstance(other, pseudo_keyframe):
            return self.value == other.value and self.frame == other.frame and self.interpolation == other.interpolation and self.handle_left == other.handle_left and self.handle_right == other.handle_right
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self.value, self.frame, self.interpolation, self.handle_left, self.handle_right))


def conform_animation(src_obj,tgt_obj,
                      src_range=[0,1],
                      tgt_starting_frame = 0,
                      actions = ['FCURVES_COPY','GPENCIL_FRAMES_COPY'], 
                      verbose = False, 
                      **kwargs ):
    '''Conform animation of source object to target object
    src : source object
    tgt : target object
    range : frame range to conform
    offset : offset in frame to apply to source object
    actions : list of actions to perform
    verbose : print debug info
    possible KWARGS :
        'fcurve_to_ignore' : list of fcurves to ignore on copy, default []
        'update_frame' : update frame on copy, default False
        'selected_curves_only' : copy only selected fcurves and gpframes, default False
    possible actions :
        'FCURVES_COPY' : copy fcurves from source to target
        'GPENCIL_FRAMES_COPY' : copy gpencil frames from source to target
        'ROOT_MATRIX_SETUP' : setup root matrix for target object. Need to be called before Fcurve copy
    '''
    if verbose : print('-conforming animation of %s to %s'%(src_obj.name,tgt_obj.name))

    #GET POTENTIAL KWARGS
    fcurve_to_ignore = kwargs.get('fcurve_to_ignore',[])
    update_frame = kwargs.get('update_frame',False)
    selected_curves_only = kwargs.get('selected_curves_only',False)
    confo_keyframe_type = kwargs.get('confo_keyframe_type',False)
    matrice_dic = kwargs.get('matrice_dic',{})
    force_gpframe_creation = kwargs.get('force_gpframe_creation',True)

    #CHECK IF ACTIONS ARE VALID
    if 'FCURVES_COPY' in actions and src_obj.animation_data.action is None :
        if verbose : print('source object has no action, removing FCURVES_COPY action')
        if 'ROOT_MATRIX_SETUP' in actions : 
            if verbose : print('ROOT_MATRIX_SETUP action makes no sence without FCURVES_COPY, removing ROOT_MATRIX_SETUP action')
            actions.remove('ROOT_MATRIX_SETUP')
        actions.remove('FCURVES_COPY')
    if 'GPENCIL_FRAMES_COPY' in actions and src_obj.type != 'GPENCIL':
        if verbose : print('source object is not a grease pencil object, removing GPENCIL_FRAMES_COPY action')
        actions.remove('GPENCIL_FRAMES_COPY')
                

    #ACTION LOOP
    for src_frame in range(src_range[0], src_range[1]) :   
        tgt_frame = src_frame + (tgt_starting_frame - src_range[0])
        for a in actions:
            match a:
                case 'ROOT_MATRIX_SETUP':#WIP
                     # ON FIRST FRAME ONLY
                    if src_frame == src_range[0]:                               
                        destination_tmatrix = t_matrix(tgt_obj)
                        if verbose: print(destination_tmatrix, '----------dest init transforms')
                        
                        source_tmatrix = get_transformation_matrix_at_frame(src_obj,src_frame)# t_matrix(src_obj)
                        source_tmatrix_inv = invert_t_matrix(source_tmatrix)

                        # tgt_obj.keyframe_insert('location', index=-1, frame=tgt_frame )
                        # tgt_obj.keyframe_insert('rotation_euler', index=-1, frame=tgt_frame )
                        # tgt_obj.keyframe_insert('scale', index=-1, frame=tgt_frame )

                    # ON OTHER FRAMES
                    else:
                        transforms = object_has_transform_keyframe(src_obj, src_frame )
                        if transforms is not None:  

                            cur_src_tmat = get_transformation_matrix_at_frame(src_obj,src_frame)#t_matrix(src_obj)                         
                            src_d_tmat = combine_t_matrix(source_tmatrix_inv, cur_src_tmat) #difference between start and current src pose

                            #apply rotation and scale of the destination object to the final location
                            rot_loc = src_d_tmat[0] 
                            
                            rotation = Euler([destination_tmatrix[1][i] * (abs(destination_tmatrix[1][i])/destination_tmatrix[1][1])                                              
                                            if destination_tmatrix[1][1] != 0.0 else 0.0
                                            for i in range(3) 
                                            ], 'XYZ')
                            
                            rot_loc.rotate(rotation)

                            
                            #rot_loc.rotate(destination_tmatrix[1])
                            matrice_dic['location'] = (rot_loc)  * destination_tmatrix[2] + destination_tmatrix[0]                         
                            #calculate rotation
                            matrice_dic['rotation_euler'] = Euler([destination_tmatrix[1][index] + src_d_tmat[1][index] for index in range(0,3)])
                            #calculate scale
                            matrice_dic['scale'] = src_d_tmat[2] * destination_tmatrix[2]

                            # assign_t_matrix([final_loc, final_rot, final_sca], tgt_obj)                                

                        else:
                            if src_frame == src_range[0] :
                                print('Warning! no transform keyframe on', tgt_obj.name, 'at frame', src_frame)
  
                case 'FCURVES_COPY':

                    if src_obj.animation_data.action is None :
                        print('WARNING! no action on %s'%src_obj.name)
                        continue

                    for src_fcurve in [fc for fc in src_obj.animation_data.action.fcurves 
                                                 if fc.data_path not in fcurve_to_ignore]:
                        if len(src_fcurve.keyframe_points) == 0 :
                            if verbose and src_frame == src_range[0]: 
                                print('no keyframe found on %s '%(src_fcurve.data_path)) 
                            continue
    
                        # Get the point to insert
                        point = get_now_kf(src_frame, src_fcurve)                        

                        if point is not None :
                            if selected_curves_only and not point.select_control_point :
                                continue
                            point_value = point.co.y
                        elif src_frame == src_range[0] and point is None :
                            continue # test for debug
                            #CHECK if there is KF on source  fcurve                            
                            point_value = src_fcurve.evaluate(src_frame)                                
                        else:
                            if verbose: print('no keyframe found for %s at frame %s'%(src_fcurve.data_path,src_frame))
                            continue
                        # get the fcurve array index
                        if fc_is_from_array(src_fcurve) :
                            array_index = src_fcurve.array_index
                        else : 
                            array_index = -1
                        # Get the tgt FCURVE
                        tgt_fcurve = tgt_obj.animation_data.action.fcurves.find(
                                                                                src_fcurve.data_path,
                                                                                index=array_index
                                                                                )
                        if tgt_fcurve is None:
                            print('WARNING! no fcurve found  on %s for %s, creating one'%(tgt_obj.name ,src_fcurve.data_path))
                            tgt_fcurve = tgt_obj.animation_data.action.fcurves.new(
                                                                                src_fcurve.data_path, 
                                                                                index=array_index
                                                                                )
                        
                        # Insert the point
                        new_kf = tgt_fcurve.keyframe_points.insert(tgt_frame, 
                                                                   point_value,
                                                                   options={'FAST'},#,'NEEDED'
                                                                   )
                        # Confo the keyframe
                        if confo_keyframe_type :
                            if point.type != "": #in  ['KEYFRAME', 'BREAKDOWN', 'MOVING_HOLD', 'EXTREME', 'JITTER'] :
                                new_kf.type = point.type
                        else:
                            new_kf.type = 'KEYFRAME'
                        # if point is not None :
                        #     confo_keyframe(point,new_kf)
                case 'GPENCIL_FRAMES_COPY':
                    if src_obj.type == 'GPENCIL' :#Grease pencil :
                        if verbose : print('copying frames from %s to %s'%(src_obj.name,tgt_obj.name))
                        src_obj.data.use_autolock_layers = False
                        # #Next wo line Should be in confo, not here
                        # sync_materials(src_obj, tgt_obj,verbose=False)
                        # sync_layers(src_obj.data, tgt_obj.data) #create missing layers
                        update_frame = False
                        for src_layer in src_obj.data.layers :
                            for tgt_layer in tgt_obj.data.layers :
                                if src_layer.info == tgt_layer.info  : #corresponding layer ! ignore locked layers : and not sel_layer.lock
                                    #if it is last frame of last layer of last object to process
                                    if (src_frame == src_range[1] - 1                                             
                                        and src_layer == src_obj.data.layers[-1] 
                                        and update_frame
                                        ):
                                        update_frame = True
                                    # src_scene.frame_set(src_frame)                                    
                                    # bpy.context.scene.frame_current = tgt_frame
                                
                                    src_gpf = get_now_gpframe(src_frame, src_layer)
                                    if src_gpf is None and force_gpframe_creation :                                       
                                        src_gpf = src_layer.active_frame

                                    if src_gpf is not None : 
                                        if selected_curves_only and not src_gpf.select :
                                            continue                                
                                        new_gpf = tgt_layer.frames.copy(src_gpf)
                                        new_gpf = move_gp_frame(tgt_layer, new_gpf, tgt_frame,update=update_frame)
                                        #MUTE FOR DEBUG: ACCESS VIOLATION
                                        # colormatch(src_obj, src_gpf, tgt_obj, new_gpf )
                                    else:
                                        print('there is no frame at frame', src_frame, 'in', src_layer.info, 'of', src_obj.name)
                                    break   
                case _:
                    print('WARNING! the action %s is not understood'%a)

def flip_animation(src_obj,regEx_pattern_flip = {},
                   src_range=[0,1],
                   tgt_starting_frame = None,
                   actions=['FCURVES','GPENCIL'],
                   ignore_unlateral_objects = True,
                   selected_curves_only = True,
                   verbose=False, 
                   **kwargs):
    '''flip animation of src_obj
            src_obj : blender object to flip
            regEx_pattern_flip : dict of regex pattern to get the suffix of the lateral objects
    '''

    #GET KWARGS
    central_flip_datapath = kwargs.get('central_flip_property','["FLIP"]')
    gp_fobidden_pattern = kwargs.get('gp_fobidden_pattern',['CTLR','HLP'])
    lyr_fobidden_pattern = kwargs.get('lyr_fobidden_pattern',['HELPERS','HLP'])


    if tgt_starting_frame is None :
        tgt_starting_frame = [bpy.context.scene.frame_current]   

    # GET FLIP SUFFIX
    suffix_ind = regex_get_lateral_index(src_obj.name, regEx_pattern_flip)
    if verbose: print('---suffix_ind: ',suffix_ind)
    if suffix_ind is None and ignore_unlateral_objects :        
        return
    # GET TGT OBJECT
    tgt_obj_name = regex_flip_name(src_obj.name, regEx_pattern_flip)
    if verbose: print('---target_object: ',tgt_obj_name)

    tgt_obj = bpy.data.objects.get(tgt_obj_name)
    if tgt_obj is None :
        print('WARNING! no target object found for %s'%src_obj.name)
        return
    
    # PASTE ANIMATION
    if verbose: print('---Conforming animation of %s '%(src_obj.name))
    conform_animation(src_obj,tgt_obj,
                      src_range=src_range,
                      tgt_starting_frame = tgt_starting_frame,
                      actions = ['FCURVES_COPY','GPENCIL_FRAMES_COPY'],
                      verbose = verbose, 
                      selected_curves_only = selected_curves_only,
                      confo_keyframe_type = True,
                      **kwargs
                       )
    
    #Check if it is not a forbidden object
    gp_flip_allowed = True
    for  gp_pattern in gp_fobidden_pattern : 
        if re.search(gp_pattern, src_obj.name) :
            if verbose: print('GP flip on  %s is forbiden'%src_obj.name)
            gp_flip_allowed = False

    # FLIP ANIMATION
    for src_frame in range(src_range[0], src_range[1]) :   
        tgt_frame = src_frame + (tgt_starting_frame - src_range[0])
        if verbose: print('----flipping src_frame',src_frame,' to tgt_frame',tgt_frame)
        for a in actions:
            if verbose: print('-----FLIP: ',a)
            match a:
                case '''FCURVES''':
                    if src_obj.animation_data and src_obj.animation_data.action :
                        for fc in src_obj.animation_data.action.fcurves :
                            if verbose: print('-------Curve %s array %s: '%(fc.data_path, get_fc_array_index(fc)))
                            #Get the keyframe point at src_frame
                            kf = get_now_kf(src_frame,fc)

                            # for kf in fc.keyframe_points :
                            if kf and kf.select_control_point :
                                # tgt_obj.keyframe_insert(data_path = fc.data_path, 
                                #                     index= get_fc_array_index(fc), 
                                #                     frame = tgt_frame)
                                #GET CORRESPONDING TARGET FC
                                tgt_fc = get_fc(tgt_obj, fc.data_path, get_fc_array_index(fc))

                                if tgt_fc is None :
                                    print('WARNING! no target fcurve found for %s'%fc.data_path)
                                    continue

                                #GET CORRESPONDING TARGET KF
                                tgt_kf = get_now_kf( tgt_frame,tgt_fc)
                                if tgt_kf is None :
                                    print('WARNING! no target keyframe found for %s'%fc.data_path)
                                    continue

                                #APPLY CORRESPONDING TRANSFORM                                   
                                match suffix_ind:      
                                    case None: 
                                        '''NO SUFFIX'''
                                        if verbose: print('-------- NO MODIFICATION')                                           

                                    case 0: 
                                        '''CENTRAL OBJECT'''   
                                        
                                        # new_kf = get_now_kf(tgt_frame, fc)
                                        # new_kf.select_control_point = False

                                        if  (   
                                            (fc.data_path == 'location' 
                                                and fc.array_index == 0) 
                                            or (fc.data_path == 'rotation_euler' 
                                                and fc.array_index == 1)
                                            ) :# TODO less hardcoding

                                            tgt_kf.co.y = tgt_kf.co.y * -1
                                            if verbose: print('--------  FLIP VALUE')
                                        elif (fc.data_path ==central_flip_datapath):
                                            if verbose:print ("-------- FLIP PROPERTY, forbidden GP flip")
                                            gp_flip_allowed = False
                                            if tgt_kf.co.y == 0 :
                                                tgt_kf.co.y = 1
                                            else :
                                                tgt_kf.co.y = 0
                                        else:
                                            if verbose: print('-------- NO MODIFICATION')        
                                    
                                            
                                        # else :                                                
                                        #     new_kf.co.y = kf.co.y

                                    case 1|-1: 
                                        '''LATERAL OBJECT'''
                                        #ALREADY HANDLED BY COFORM ANIMATION
                                        if verbose: print('-------- NO MODIFICATION')   
                                        # for ffc in tgt_obj.animation_data.action.fcurves :
                                        #     if (ffc.data_path == fc.data_path 
                                        #         and ffc.array_index == fc.array_index
                                        #         ):
                                        #         new_kf = get_now_kf(tgt_frame, ffc)
                                        #         new_kf.co.y = kf.co.y
                                        #         new_kf.select_control_point = False                                                    
                                        #         break
        
                case '''GPENCIL''' if gp_flip_allowed :
                    if src_obj.type == 'GPENCIL' :   
                        
                                             
                        for src_layer in src_obj.data.layers :
                            #check if a fobidden patern is in layer name
                            if any(pattern in src_layer.info for pattern in lyr_fobidden_pattern) :
                                if verbose: print('this layer is forbidden for GP flip')
                                continue
                            match suffix_ind:
                                case None: 
                                    '''NO SUFFIX'''                                   

                                case 0: 
                                    '''CENTRAL OBJECT'''   
                                    #TODO checkif there is a flip property on tgt_obj
                                    if not gp_flip_allowed :
                                        if verbose: print('this object has a FLIP property. GP FLIP forbidden')
                                        # tgt_obj[central_flip_property] = - tgt_obj[central_flip_property]
                                    else:
                                    #GET CORRESPONDING TARGET LAYER
                                        tgt_layer = tgt_obj.data.layers.get(src_layer.info)
                                        if tgt_layer is None :
                                            print('WARNING! no target layer found for %s'%src_layer.info)
                                            continue
                                        
                                        src_gp_frame = get_now_gpframe(src_frame, src_layer)
                                        if src_gp_frame is None :
                                            # print('WARNING! no source frame found for %s'%src_frame)
                                            continue
                                        if src_gp_frame.select :
                                            tgt_gp_frame = get_now_gpframe(tgt_frame, tgt_layer) 

                                            if tgt_gp_frame is None :
                                                print('WARNING! no target frame found for %s'%tgt_frame)
                                                continue 

                                            tgt_gp_frame.select = False
                                            for stroke in tgt_gp_frame.strokes :  
                                                #store point.co[0] in an array
 
                                                #create a pseudo stroke
                                                original_stroke = Pseudo_stroke(stroke)
                                                
                                                original_stroke.points.reverse()
                                                if original_stroke.use_cyclic :
                                                    original_stroke.points.flip_start_point()
                                                original_stroke.points.flip()

                                                #conform stroke to reversed
                                                # stroke_confo(original_stroke, stroke, verbose = verbose)

                                                                                              
                                                for i,point in enumerate(stroke.points):                                                     
                                                    
                                                    point_confo(original_stroke.points[i], point)
                                                
                                                stroke.points.update()

                                                
                                                



                            

def get_now_kf(current_frame, fcurve):
    '''returns keyframe at current_frame, if it exists
    args:
        current_frame (int): the frame to look for
        fcurve (fcurve): the fcurve to look in'''
    if fcurve is not None :
        for keyframe in fcurve.keyframe_points : 
            if keyframe.co.x == current_frame :
                return keyframe
    return None
  
def get_fc(tgt,data_path, array_index) :
    '''returns fcurve from tgt object, if it exists'''
    #REMOVE '[' ']' and """" from data_path
    # data_path = data_path.replace('[','').replace(']','').replace('"','')

    if tgt.animation_data and tgt.animation_data.action :
        for fc in tgt.animation_data.action.fcurves :
            if( fc.data_path == data_path and 
            (fc.array_index == array_index or array_index == -1)) :
                return fc
    return None
    
def refresh_dopesheet() : #this causes so many crashes :(
    print('refresh_dopesheet')
    for area in bpy.context.screen.areas:
        if area.type == 'DOPESHEET_EDITOR' and area.ui_type == 'DOPESHEET':
            ctx = bpy.context.copy()
            ctx['area'] = area # not actually needed...
            ctx['region'] = area.regions[-1] # ... just be nice
            #with bpy.context.temp_override(area=area, region=area.regions[-1]):  
            #bpy.ops.action.select_all(ctx, action='SELECT')
            bpy.ops.transform.transform(ctx, mode='TIME_TRANSLATE', value=(0, 0, 0, 0))
            #bpy.ops.action.select_all(action='DESELECT')
            break
    print('ok')
   
def create_anim_data(object):
    '''makes sure a given object has animation data, and at least the 9 basic transforms Fcurves
    args : object = the object you want to ensure anim data'''
    if object.animation_data is None :
            #print('create anim_data')
            object.animation_data_create()
    if object.animation_data.action is None :
        #print('create Action')
        object.animation_data.action = bpy.data.actions.new(name = object.name + 'Action')
    animloc = False
    animrot = False
    animsca = False
    for fcurve in object.animation_data.action.fcurves :
        if fcurve.data_path == 'location' :
            animloc = True
        if fcurve.data_path == 'rotation_euler' :
            animrot = True
        if fcurve.data_path == 'scale' :
            animsca = True
    if animloc == False :
        object.keyframe_insert(data_path = 'location', frame = 1, index = -1, group = 'Object Transforms')
        #print('keyframe location at frame 1')
    if animrot == False :
        object.keyframe_insert(data_path = 'rotation_euler', frame = 1, index = -1, group = 'Object Transforms')
        #print('keyframe rotation at frame 1')
    if animsca == False :
        object.keyframe_insert(data_path = 'scale', frame = 1, index = -1, group = 'Object Transforms')
            #print('keyframe scale at frame 1')

def create_missing_fcurves(tgt_obj, src_obj):
    for src_fcurve in [fc for fc in src_obj.animation_data.action.fcurves if fc.data_path not in ['location','rotation_euler','scale']]:
        found = False
        for fcurve in [fc for fc in tgt_obj.animation_data.action.fcurves if fc.data_path not in ['location','rotation_euler','scale']]:
            if fcurve.data_path == src_fcurve.data_path and fcurve.array_index == src_fcurve.array_index : #match
                found = True
                break
        if not found :
            new_fc = tgt_obj.animation_data.action.fcurves.new(src_fcurve.data_path, index = src_fcurve.array_index)

def fc_is_from_array(fcurve) :
    if len([fc for fc in fcurve.id_data.fcurves if fc.data_path == fcurve.data_path]) > 1 :
        return True
    else :
        return False
    
def get_fc_array_index(fcurve) :
    '''returns the array index of a fcurve, or -1 if it's not from an array'''
    if not fc_is_from_array(fcurve) :
        fc_index = -1
    else :
        fc_index = fcurve.array_index
    return fc_index

def get_dopesheet_selected(dopesheet_types = ['DOPESHEET_EDITOR','GREASEPENCIL'],
                           selected_only=False,
                           verbose = False) :
    '''returns the time extreme of the keyframe currently selected in the dopesheet
    args : verbose = if True, prints debug info'''    
    min_frame = None
    max_frame = None
    selected_object_keys = []
    if verbose :print('---- Getting selected frame range and object in the dopesheet')
    #Get blender dopesheet summary 
    # collection = sheet_space = None
    # for area in bpy.context.screen.areas :
    #     if  collection is None and sheet_space is None: 
    #         if area.type == 'DOPESHEET_EDITOR' :
    #             if verbose :
    #                 if verbose:print('area type : %s'%area.type)
    #             for region in area.regions :
    #                 if region.type == 'WINDOW' :
    #                     if verbose :
    #                         if verbose:print('region type : %s'%region.type)
    #                     for space in area.spaces :
    #                         if space.type in dopesheet_types :
    #                             if verbose :
    #                                 if verbose:print('space type : %s'%space.type)
    #                             if space.mode in ['DOPESHEET','GPENCIL']:
    #                                 dopesheet = space.dopesheet
    #                                 if  dopesheet.filter_collection:
    #                                     collection =  dopesheet.filter_collection.all_objects
                                        
    #                                 else:#ALL OBJECTS
    #                                     #selected object only
                                                                         
    #                                     collection = bpy.context.scene.collection.all_objects
    #                                     if dopesheet.show_only_selected == True :
    #                                         collection = bpy.context.selected_objects
    #                                 sheet_space=space
    #                                 break
    #                             else:
    #                                 if verbose:print('unhandled space mode : %s'%space.mode)
    # 
    collection,sheet_space = get_dopesheet_displayed(dopesheet_types = dopesheet_types,verbose = verbose)
    for obj in collection :
        # if selected_only:
        #     if not obj.select_get() :
        #         continue
        #check if object is hidden
        # if obj.hide_viewport :
        #     if verbose:print('object %s is hidden'%obj.name)
        #     continue                              
        if sheet_space.mode == 'DOPESHEET' :
            if verbose:print('dopesheet mode : DOPESHEET')
        #get all object selected in dopesheet
            if not obj.animation_data or not obj.animation_data.action :
                continue
            for channel in obj.animation_data.action.fcurves :
                for keyframe in channel.keyframe_points :
                    
                    if keyframe.select_control_point :
                        if obj.name not in selected_object_keys :
                            selected_object_keys.append(obj.name)
                        if verbose :
                            print('on object %s, channel %s, keyframe %s is selected'%(obj.name, channel.data_path, keyframe.co.x))
                        if min_frame is None or keyframe.co.x < min_frame  :
                            min_frame = int(keyframe.co.x)
                        if max_frame is None or keyframe.co.x > max_frame :
                            max_frame = int(keyframe.co.x)
        elif sheet_space.mode == 'GPENCIL' :
            if verbose:print('dopesheet mode : GPENCIL')
            for layer in obj.data.layers :
                for frame in layer.frames :
                    # for stroke in frame.strokes :
                        if frame.select :
                            if obj.name not in selected_object_keys :
                                selected_object_keys.append(obj.name)
                            if verbose :
                                print('on object %s, frame %s is selected'%(obj.name, frame.frame_number))
                            if min_frame is None or frame.frame_number < min_frame  :
                                min_frame = int(frame.frame_number)
                            if max_frame is None or frame.frame_number > max_frame :
                                max_frame = int(frame.frame_number)

    
    # for action in bpy.data.actions:
    #     #check if actions object is visible
        
    #     for fcurve in action.fcurves: 
    #         if fcurve.select :               
    #             for keyframe in fcurve.keyframe_points :
    #                 if keyframe.select_control_point :
    #                     if min_frame is None or keyframe.co.x < min_frame  :
    #                         min_frame = int(keyframe.co.x)
    #                     if max_frame is None or keyframe.co.x > max_frame :
    #                         max_frame = int(keyframe.co.x)
    # #Iterate troug all file keyframes and check if they are selected
    # for actions in  bpy.data.actions :
    #     for fcurve in actions.fcurves :
    #         for keyframe in fcurve.keyframe_points :
    #             if keyframe.select_control_point :
    #                 if min_frame is None or keyframe.co.x < min_frame  :
    #                     min_frame = int(keyframe.co.x)
    #                 if max_frame is None or keyframe.co.x > max_frame :
    #                     max_frame = int(keyframe.co.x)
    # #Iterate trough all gp keyframes and check if they are selected
    # for gp in bpy.data.grease_pencils :        
    #     for layer in gp.layers :
    #         for frame in layer.frames :
    #             #if visible and selected                
    #             if frame.select :
    #                 if min_frame is None or frame.frame_number < min_frame  :
    #                     min_frame = int(frame.frame_number)
    #                 if max_frame is None or frame.frame_number > max_frame :
    #                     max_frame = int(frame.frame_number)

    if verbose:
        print('min_frame : ' + str(min_frame))
        print('max_frame : ' + str(max_frame))
    return min_frame, max_frame,selected_object_keys

def get_dopesheet_displayed(dopesheet_types = ['DOPESHEET_EDITOR','GREASEPENCIL'],verbose = False) :
    collection = sheet_space = None
    for area in bpy.context.screen.areas :
        if  collection is None and sheet_space is None: 
            if area.type == 'DOPESHEET_EDITOR' :
                if verbose :
                    if verbose:print('area type : %s'%area.type)
                for region in area.regions :
                    if region.type == 'WINDOW' :
                        if verbose :
                            if verbose:print('region type : %s'%region.type)
                        for space in area.spaces :
                            if space.type in dopesheet_types :
                                if verbose :
                                    if verbose:print('space type : %s'%space.type)
                                if space.mode in ['DOPESHEET','GPENCIL']:
                                    dopesheet = space.dopesheet

                                    show_only_selected = dopesheet.show_only_selected
                                    show_hidden = dopesheet.show_hidden

                                    if  dopesheet.filter_collection:
                                        collection =  dopesheet.filter_collection.all_objects
                                        
                                    else:#ALL OBJECTS
                                        #selected object only
                                                                            
                                        collection = bpy.context.scene.collection.all_objects
                                        
                                    sheet_space=space
                                    break
                                else:
                                    if verbose:print('unhandled space mode : %s'%space.mode)


    if show_only_selected == True :
        #remove unselected objects from collection
        collection = [obj for obj in collection if obj.select_get()]
        # collection = bpy.context.selected_objects
    if show_hidden == False :
        #remove hidden objects from collection
        collection = [obj for obj in collection if not obj.hide_viewport]
        # collection = bpy.context.visible_objects

    return collection, sheet_space
       

def get_displayed_objects() : #list objects visible in the dopesheet
    screen = bpy.context.screen
    scene = bpy.context.scene
    window = bpy.context.window_manager
    window.ds_data.filter_displayed_objects.clear()
    for area in screen.areas :
        if area.spaces[0].type == 'DOPESHEET_EDITOR' and area.spaces[0].mode == 'DOPESHEET':
            if area.spaces[0].dopesheet.show_only_selected == True :
                #print(bpy.context.selected_objects)
                for obj in bpy.context.selected_objects :
                    if obj.type == 'GPENCIL' :
                        window.ds_data.filter_displayed_objects.append(obj)
            else :
                if area.spaces[0].dopesheet.show_hidden == False :
                    for obj in bpy.context.visible_objects : 
                        if obj.type == 'GPENCIL' :  
                            window.ds_data.filter_displayed_objects.append(obj)
                else :
                    for obj in scene.objects :
                        if obj.type == 'GPENCIL' :
                            window.ds_data.filter_displayed_objects.append(obj)
            break
    return window.ds_data.filter_displayed_objects

def get_selected_channels(): #list all slected gp animation channels (fcurves)
    result = []
    for ob in get_displayed_objects():
        if  ob.animation_data and ob.animation_data.action and 'Grease Pencil frames' in ob.animation_data.action.groups :
            group = ob.animation_data.action.groups['Grease Pencil frames']
            for channel in group.channels :
                if channel.select :
                    result.append((ob, channel.data_path[2:-2]))
    if len(result) > 0:
        return result
    else : 
        return [None]

def get_locked_channels() :#list locked animation channels (fcurves)
    #print('get_locked')
    result = []
    for ob in get_displayed_objects():
        if  ob.animation_data and ob.animation_data.action and 'Grease Pencil frames' in ob.animation_data.action.groups :
            group = ob.animation_data.action.groups['Grease Pencil frames']
            for channel in group.channels :
                if channel.lock :
                    result.append((ob, channel.data_path[2:-2]))
    #print('get_locked end')
    return result



def deselect_all_keyframes(ob):
    if ob.type == 'GPENCIL' :
        if ob.data :
            for layer in ob.data.layers :
                for frame in layer.frames :
                    frame.select = False
    if not ob.animation_data:
        return
    
    action = ob.animation_data.action
    if action:
        # deselect all fcurves
        action.fcurves.foreach_set(
                "select",
                (False,) * len(action.fcurves),
                )
        # deselect all keyframes
        for fc in action.fcurves:
            for select in ("select_control_point",
                           "select_left_handle",
                           "select_right_handle",
                               ):
                fc.keyframe_points.foreach_set(
                        select, 
                        [False] * len(fc.keyframe_points)
                        )
    
def object_has_transform_keyframe(object, frame_number, pass_locked = False):
    '''check if an object has transfom keyframe on a defined frame number
    returns : a list of transform fcurves'''
    result = []
    if object.animation_data and object.animation_data.action:
        for fcurve in object.animation_data.action.fcurves :
            if get_now_kf(frame_number, fcurve) is not None :
                if fcurve.data_path in ['location', 'rotation_euler', 'scale'] and fcurve.data_path not in result :
                    if pass_locked == True :
                        if fcurve.lock :
                            continue
                    result.append(fcurve.data_path)
    if len(result) == 0 :
        return None
    else :
        return result


def get_driver(tgt, data_path, index = -1, verbose = False):
    '''returns the driver of an object based on datapath and index'''
    if tgt.animation_data is None or tgt.animation_data.drivers is None :
        return None
    
    for driver in tgt.animation_data.drivers :
        if driver.data_path == data_path and driver.array_index == index :
            return driver
    return None

def copy_driver(src, tgt, tgt_driver = None,verbose = False):
    try:
        if tgt_driver is None :
            if tgt.animation_data is None:
                tgt.animation_data_create()
            print("Driver copy %s on %s"%(src.data_path,tgt.name) )
            try:
                tgt_driver = tgt.driver_add(src.data_path)#,src.array_index
            except:
                print("Driver add failed, %s, trying to get existing driver"%(src.data_path) )

        if isinstance(tgt_driver, list):
            driver_list = tgt_driver
        else:
            driver_list = [tgt_driver]
        for driver in driver_list:  
            driver.driver.expression = src.driver.expression
            for src_var in src.driver.variables:
                tgt_drv = driver.driver
                copy_driver_variables(src_var, tgt_drv,verbose=verbose)
            try:
                confo_fcurve(src, driver)
            except Exception as e:
                print("Driver modifiers copy failed on %s, error:%s"%(src.data_path,e) )
    except Exception as e:
        print("Driver %s copy failed on %s, error %s"%(src.data_path,tgt.name,e) )

def copy_driver_variables(src_var, tgt_drv, verbose = False):
    tgt_var=None
    #if target variable doesn't exist, create it
    for var in tgt_drv.variables:
        if var.name == src_var.name:
            tgt_var = var
            break
    if not tgt_var:
        tgt_var = tgt_drv.variables.new()
    tgt_var.type = src_var.type
    tgt_var.name = src_var.name
    try:
        #v2.targets[0].data_path = tgt_ob.data.data_path
        for i in range(len(src_var.targets)):  
            src_target = src_var.targets[i]  
            tgt_target = tgt_var.targets[i]        
            name = src_target.data_path

            tgt_target.data_path = name
            #ERROR HERE, tgtobj isn't always tgt_ob
            target_name = src_target.id.name
            #get the object in tgt_ob's scene
            tgtobj = bpy.data.objects.get(target_name)
            tgt_target.id = tgtobj
            
            for attr in ['transform_type', 
                         'transform_space', 
                         'id_type',
                         'rotation_mode', 
                         'bone_target'
                         'context_property']:
                # check if dir exist 
                if hasattr(src_target, attr) and hasattr(tgt_target, attr):
                    if  getattr(src_target, attr)!= getattr(tgt_target, attr):
                        try:
                            setattr(tgt_target, attr, getattr(src_target, attr))
                        except:
                            print("WARNING!!! %s copy failed on %s"%(attr,src_target.id) )
            
        
    except:
        print("Variable copy failed, %s"%(src_var.targets[0].id) )

def copy_driver_modifiers(src,tgt):
    if src.modifiers :           
        for mod in src.modifiers:
            copy = tgt.modifiers.new(mod.type)
            for prop in mod.bl_rna.properties:
                if not prop.is_readonly:
                    setattr(copy, prop.identifier, getattr(mod, prop.identifier))


def confo_fcurve(src,tgt):
    """conform a tgt fcurve to src fcurve
    """
    for m in tgt.modifiers:
        tgt.modifiers.remove(m)

    
    # array_index
    tgt.array_index = src.array_index
    # auto_smoothing
    tgt.auto_smoothing = src.auto_smoothing    
    # color
    tgt.color = src.color
    #color_mode
    tgt.color_mode = src.color_mode
    #data_path

    #driver (readonly)

    # extrapolation
    tgt.extrapolation = src.extrapolation
    #group
    # tgt.group = src.group
    #hide
    tgt.hide = src.hide
    #lock
    tgt.lock = src.lock
    #keyframe_points(readonly)
    for k in src.keyframe_points :
        new_k = tgt.keyframe_points.insert(k.co.x, k.co.y)
        confo_keyframe(k,new_k)
    #modifier (readonly)
    copy_driver_modifiers(src,tgt)
    
def confo_keyframe(src,tgt, attributes = ['amplitude', 
                                       'back', 
                                       'co', 
                                       'co_ui', 
                                       'easing', 
                                       'handle_left', 
                                       'handle_left_type', 
                                       'handle_right', 
                                       'handle_right_type', 
                                       'interpolation', 
                                       'period', 
                                       'select_control_point', 
                                       'select_left_handle',
                                       'select_right_handle',
                                       'type']):
    """conform a tgt keyframe to src keyframe"""  
    for a in attributes :
        match a :
            case 'co':
                tgt.co = src.co.copy()
            case 'force_constant_interpolation':
                #set frame interpolation to constant
                tgt.interpolation = 'CONSTANT'
            case _:
                try:
                    setattr(tgt, a, getattr(src, a))
                except:
                    print("Confo keyframe failed on %s, attribute %s"%(tgt,a) )             


def enforce_keyframe(object,i,verbose=False):
    '''enforce a keyframe on an object at a given frame
    args:
        object (object): the object to enforce keyframe on
        i (int): the frame number
        '''
    if object.type == 'GPENCIL' :
        for layer in object.data.layers :
            if get_now_gpframe(i, layer) is None :
                if not layer.active_frame :
                    print('warning : no frame in layer', layer.info , 'of GP ',object.name, '; adding an empty one')
                    new_kf = layer.frames.new(i)
                    new_kf.frame_number = i
                    
                    
                else :
                    # if verbose:print('inserting frame', i,
                    #                  'in layer', layer.info ,
                    #                  'of GP ',object.name, '; copying previous frame')
                    # new_kf = layer.frames.copy(layer.active_frame)
                    new_kf = layer.frames.new(i)
                    frame_confo(layer.active_frame, new_kf)
                    new_kf.frame_number = i
                for stroke in new_kf.strokes :
                    stroke.points.update()
            pass
    if object.animation_data is not None and object.animation_data.action is not None :
        for fcurve in object.animation_data.action.fcurves :
            if get_now_kf(i, fcurve) is None :
                if fc_is_from_array(fcurve):
                    object.keyframe_insert(data_path = fcurve.data_path, frame = i, index = fcurve.array_index)
                else:
                    object.keyframe_insert(data_path = fcurve.data_path, frame = i,index = -1)
    else:
        if verbose:print('warning : no animation data on object', object.name)
        
#MATRIX OPERATION
def get_transformation_matrix_at_frame(object, frame) :
    '''return the transformation matrix of an object at a given frame'''
    # Sauvegarde de la frame courante
    current_frame = bpy.context.scene.frame_current

    # Déplacement à la frame voulue
    bpy.context.scene.frame_set(frame)

    # Récupération de la matrice de transformation
    transfo_matrix = t_matrix(object)

    # Retour à la frame courante
    bpy.context.scene.frame_set(current_frame)

    return transfo_matrix

def t_matrix(object) :
    '''return the transformation matrix of an object as a list of 3 vectors (location, rotation, scale)'''
    return [Vector(object.location), Euler(object.rotation_euler), Vector(object.scale)]

def assign_t_matrix(t_matrix, object) :
    '''assign a transformation matrix to an object from a list of 3 vectors (location, rotation, scale)'''
    object.location = t_matrix[0]
    object.rotation_euler = t_matrix[1]
    object.scale = t_matrix[2]

def invert_t_matrix(t_matrix) : #equivalent de matrix.invert()
    '''invert a transformation matrix formed of a list of 3 vectors (location, rotation, scale)'''
    # Extraction des composantes individuelles des matrices
    loc, rot, sca = t_matrix

    # Inversion des composantes
    loc = Vector([-loc[i] for i in range(3)])
    rot = Euler([-rot[i] for i in range(3)], 'XYZ')
    sca = Vector([1 / sca[i] if sca[i] != 0 else 0 for i in range(3)])

    return [loc, rot, sca]

def combine_t_matrix(t_matrix_1, t_matrix_2, verbose=None): #equivalent de l'opération matrix1 @ matrix2 
    '''combine 2 transformation matrices formed of a list of 3 vectors (location, rotation, scale)'''

    loc = t_matrix_2[0]+t_matrix_1[0]
    loc *= t_matrix_1[2]
    loc.rotate(t_matrix_2[1])

    rot = Euler([t_matrix_1[1][index] + t_matrix_2[1][index] for index in range(3)])
    sca = t_matrix_1[2] * t_matrix_2[2]
    if verbose: print([loc, rot, sca])
    return [loc, rot, sca]


#GETTER
def get_animation_kf_dic(src_obj,cible = ['FC','GP'],
                         frame_range = [0,100],
                         extend_grid_1 = False,
                         verbose = False,
                         pseudo = False,
                         **kwargs
                         ):
    """return a dictionnary of all keyframes of an object
    shape is dic['KF'][data_path][array_index] = [kf1,kf2,...]
    ARGS:
        src_obj : object to get keyframes from
        cible : list of kf type to get default is ['FC','GP']   
        frame_range : range of frames to get keyframes from
        extend_grid_1 : if True, extend the grid of one line
        pseudo : if True, get pseudo keyframes instead of real ones
    KWARGS:
        gp_fobidden_pattern : list of pattern to exclude from GPFRAME       
            """
    dic = {}

    #GET KWARGS
    gp_fobidden_pattern = kwargs.get('gp_fobidden_pattern',[])
    offset = kwargs.get('offset',0)

    #PROCESS
    if verbose: print('-Getting animation keyframes of %s'%src_obj.name)
    for pattern in gp_fobidden_pattern : 
                    if re.search(pattern, src_obj.name) :
                        if verbose: print('---object name match %s forbidden pattern: Object GPFRAME not stored'%pattern)
                        cible.remove('GP')
    for c in cible :
        dic[c] = {}
        if verbose: print('--Getting %s keyframes'%c)
        match   c :
            case  'FC' :
                if src_obj.animation_data is None or src_obj.animation_data.action is None:
                    if verbose: print('---object has no animation data')
                    continue
                for src_fcurve in src_obj.animation_data.action.fcurves :  
                    current_list = dic[c].get(src_fcurve.data_path,{})
                    #GET KEYS LISTE
                    array_index =  get_fc_array_index(src_fcurve)                     
                    src_kf_list = [kf for kf in src_fcurve.keyframe_points if frame_range[0] <= kf.co.x <= frame_range[1]]
                    if pseudo :
                        #make a list of pseudo_keyframes from src_kf_list
                        src_kf_list = [pseudo_keyframe(kf) for kf in src_kf_list]
                    if extend_grid_1 : 
                        src_kf_list+=src_kf_list
                    #ADD TO DIC
                    current_list[array_index] = src_kf_list
                    dic[c][src_fcurve.data_path] = current_list       
                    
            case 'GP' :
               if not src_obj.type == 'GPENCIL' :
                    if verbose: print('---object is not a grease pencil object')
                    continue               
                        
               for src_layer in src_obj.data.layers:
                    current_list = dic[c].get(src_layer.info,{})       
                    #GET SRC GPFRAME LIST 
                    src_gpframe_list = [kf for kf in src_layer.frames if frame_range[0] <= kf.frame_number <=frame_range[1]]
                    if pseudo :
                        src_gpframe_list = [Pseudo_gp_frame(kf) for kf in src_gpframe_list]
                        # pseudo_frames =  bpy.data..new('emptyMesh')
                        #make a list of pseudo_keyframes from src_kf_list                        
                        # for i in  range(len(src_gpframe_list)) :
                        #     src_gpframe_list[i] = frame_confo(src_gpframe_list[i].copy(), src_gpframe_list[i])
                        pass
                    if extend_grid_1 : 
                        src_gpframe_list += src_gpframe_list
                    # try:
                    #     self.src_gp_keys[obj_ind][1].append((src_layer, src_gpframe_list)) 
                    # except IndexError as e:
                    #     print('IndexError : ' + str(e))
                    #     pass 
                    #ADD TO DIC
                    dic[c][src_layer.info] = src_gpframe_list             
        if verbose: print('---%s keyframes found'%len(dic[c]))                                    

                
    return dic

def recursive_merge_dict(dict1, dict2,
                         keep_both=True,
                         master_1=True,
                         master_2=True,
                         level_mark = '',
                         verbose=True):
    # if verbose:print('recursive_merge_dict')
    for key, val in dict1.items():
        if verbose:print(level_mark+'-merging key',key)
        if type(val) == dict:
            if key in dict2 and type(dict2[key] == dict):
                recursive_merge_dict(dict1[key], 
                                     dict2[key],
                                     keep_both=keep_both,
                                     master_1=master_1,
                                     master_2=master_2,
                                     level_mark = level_mark+'-',
                                     verbose=verbose)
        else:
            if key in dict2:
                if keep_both and type(val)==list:
                    if master_2 and len(dict2.get(key))>len(dict1.get(key,[]))==0:
                        if verbose:print(level_mark+'no list in dict2 %s, skipping'%key)
                        continue                    
                    dict1[key] = dict1[key]+dict2[key]
                else:
                    dict1[key] = dict2[key]
            else:
                if master_2:
                    if verbose:print(level_mark+'key isnt in dict2 %s, removing'%key)
                    dict1.pop(key)
                    continue
                

    for key, val in dict2.items():
        if not key in dict1:
            if master_1:
                continue
            dict1[key] = val

    return dict1

def vector_stroke_interpolation_calculation(tgt_stroke,src_strokes_array,coef_array):
    """using numpy, calculate the interpolation of a gp stroke by ponderating each src strok point 
    with there corresponding coef and set tgt to the result
     args :
        tgt_stroke : the target stroke
        src_strokes_array : a list of n strokes
        coef_array : an array of n coef """
    #CHECK IF SRC STROKES AND COEF ARE THE SAME LENGHT
    if len(src_strokes_array) != len(coef_array):
        print('src_strokes_array and coef_array must be the same lenght')
        return
    
    #BUILD SOURCE POINTS ARRAY WITH 0 FOR EMPTY POINTS
    src_points = np.zeros((len(src_strokes_array),len(tgt_stroke.points),3))
    for i,s in enumerate(src_strokes_array):
        for j,p in enumerate(s.points):
            src_points[i,j] = p.co

    #GET COEF
    coef_array = np.array(coef_array)
    #CALCULATE INTERPOLATION
    tgt_points = np.sum(src_points*coef_array[:,None,None],axis=0)

    #SET TGT STROKE
    for i,p in enumerate(tgt_stroke.points):
        p.co = tgt_points[i]
    return tgt_stroke

def old_interpolate_gp_strokes(tgt_stroke,src_strokes_array,coef_array):
    
    
    for l, point in enumerate(tgt_stroke.points):            
        #Try to get point from each stroke
        existing_points = []
        existing_coefficients = []
        for k, stroke in enumerate(src_strokes_array):
            try:
                src_point = stroke.points[l]
            except IndexError:
                continue
            existing_points.append(src_point)
            existing_coefficients.append(coef_array[k])
        if len(existing_points) != len(src_strokes_array) or len(existing_points) == 0:
            continue
        # If only one point found o if they are identicall, set tgt point to src point
        if all(existing_points[0].co == p.co for p in existing_points):
            point.co = existing_points[0].co
            continue
        # If more than one point found, interpolate
        try:
            point.co = sum([p.co * coef for p, coef in zip(existing_points, existing_coefficients)])
        except :
            pass
            
    # # try:
    #     # if the coordinate are different, interpolate
    #     if (GP_TL.strokes[k].points[l].co != GP_DL.strokes[k].points[l].co or 
    #         GP_TL.strokes[k].points[l].co != GP_DR.strokes[k].points[l].co or 
    #         GP_TL.strokes[k].points[l].co != GP_TR.strokes[k].points[l].co) :
    #         co_TL = GP_TL.strokes[k].points[l].co
    #         co_DL = GP_DL.strokes[k].points[l].co
    #         co_DR = GP_DR.strokes[k].points[l].co
    #         co_TR = GP_TR.strokes[k].points[l].co

    #         point.co = (co_TL * coef_TL+
    #                     co_DL * coef_DL+
    #                     co_DR * coef_DR+
    #                     co_TR * coef_TR)   
    #         update = True                            
    # except :
    #     pass


def interpolate_gp_strokes(tgt_strokes,
                           src_gp_key_list,
                           src_frame_list = [0,1,2,3],
                           src_frame_coef = [0.25,0.25,0.25,0.25],
                           clip=False,   
                           last_clip_gpframe=None,  
                           build = True,                  
                           verbose = False):
    missing_index = []
    strokes_array_list = []    

    # BUILD stroke collection LIST
    if build:
        for ind in range(len(src_frame_list)):
            frame = src_frame_list[ind]
            #avoid index out of range if  src_curve_key_list[frame] doesnt exist
            if frame >= len(src_gp_key_list):
                missing_index.append(ind)
                continue
            strokes_array_list.append(src_gp_key_list[frame])
        
        #REMOVE MISSING INDEX COEF  (be carefll to pop in correct order)
        for ind in missing_index:
            src_frame_coef.pop(ind)   

        #NORMALIZE COEF
        src_frame_coef = [x/sum(src_frame_coef) for x in src_frame_coef]

    else:
        strokes_array_list = src_gp_key_list

    higher_coeff_index = src_frame_coef.index(max(src_frame_coef))
    clip_gpframe = src_gp_key_list[src_frame_list[higher_coeff_index]]
    if (not clip 
        # or higher_coeff_index<=0.95 
        ):
        
        
        for k, stroke in enumerate(tgt_strokes):
            
            try:
                tgt_stroke_array = [s[k] for s in strokes_array_list]
                # vector_stroke_interpolation_calculation(stroke,
                #                                         tgt_stroke_array,
                #                                         src_frame_coef
                #                                         )
                vector_stroke_interpolation_calculation(stroke,
                                                        tgt_stroke_array,
                                                        src_frame_coef
                                                        )
            except Exception as e:
                
                # print(e, 'on stroke', k, 'on layer', tgt_layer_name, 'on object', tgt_obj_name)
                continue

        
    
        
    return clip_gpframe
         
                  
def interpolate_co_y(src_curve_key_list,
                    src_frame_list = [0,1,2,3],
                    src_frame_coef = [0.25,0.25,0.25,0.25],
                    clip=False,
                    build = True,                       
                    verbose = False):

    """interpolate the y coordinate of a target keyframe using a list of source keyframes
    args :
        src_curve_key_list : a list of source keyframes
        src_frame_list : a list of source keyframes index to use for interpolation
        src_frame_coef : a list of coef to use for interpolation
        clip : if True, clip the result to the highest coef value of the source keyframes
        verbose : if True, print some info
        return : the interpolated y coordinate"""
    

    missing_index = []
    kf_list = []
    co_y = 0

    # BUILD SRC FCURVE KEY LIST
    if build:
        for ind in range(len(src_frame_list)):
            frame = src_frame_list[ind]
            #avoid index out of range if  src_curve_key_list[frame] doesnt exist
            if frame >= len(src_curve_key_list):
                missing_index.append(ind)
                continue
            kf_list.append(src_curve_key_list[frame])
    
        #REMOVE MISSING INDEX COEF  (be carefll to pop in correct order)
        if len(kf_list)>len(src_frame_coef):
            for ind in reversed(missing_index):
                src_frame_coef.pop(ind)
    else   :
        kf_list = src_curve_key_list


    #NORMALIZE COEF
    src_frame_coef = [x/sum(src_frame_coef) for x in src_frame_coef]

    higher_coeff_index = src_frame_coef.index(max(src_frame_coef))
        

    if not clip  : 
        #CALCULATE CO.Y
        
        for ind in range(len(kf_list)):
            co_y += kf_list[ind].co.y * src_frame_coef[ind]


    else : #clip transforms
        
        clip_kf = kf_list[higher_coeff_index]
        co_y = clip_kf.co.y
    return co_y