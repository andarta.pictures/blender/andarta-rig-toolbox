# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy

from .import_utils import ensure_lib

optional_lib= [
    ['scipy','scipy'],
    ]
for lib in optional_lib :
    has_lib = ensure_lib(lib[0],lib[1])
    if not has_lib:
        print("Warning: Could not import nor install " + lib[0] +'\n'
              + 'Some features may not work properly \n' +              
                'Try again and if this error persist install dependencies manually')
        

import numpy as np
from bpy_extras import view3d_utils
from scipy.spatial.distance import cdist

def get_flipped_start_point_indice(pseudo_points):
    points = np.array([point.co for point in pseudo_points])
    start_point = points[0]
    min_x = min([p[0] for p in points])
    max_x = max([p[0] for p in points])
    min_z = min([p[2] for p in points])
    max_z = max([p[2] for p in points])
    start_state_x = (start_point[0] - min_x) / (max_x - min_x)
    start_state_z = (start_point[2] - min_z)/ (max_z - min_z)
    approx_new_start_co = (((1-start_state_x)*(max_x - min_x)) + min_x ,0, (start_state_z*(max_z - min_z)) + min_z)
    distances = cdist([approx_new_start_co], points)
    return np.argmin(distances)

def print_m(text, matrix, n=3) :
    """Print numpy array with a label and shape
    - text : label to be displayed
    - matrix : np.array to be displayed
    - n : rounds the values to n decimals"""

    print(text, matrix.shape)

    with np.printoptions(precision=n, suppress=True):
        print(matrix)

def normalize_vector(vector) :
    """Returns normalized vector.
    - vector : 1D np.array"""

    N = np.linalg.norm(vector)
    if N == 0 :
        return vector
    else :
        return vector/N

def location_to_region(worldcoords, region=None, r3d=None):
    """Short handle to call view3d_utils.location_3d_to_region_2d in a more practical manner."""

    if region is None :
        region = bpy.context.region
    if r3d is None :
        r3d = bpy.context.space_data.region_3d

    return view3d_utils.location_3d_to_region_2d(region, r3d, worldcoords)

def region_to_location(viewcoords, depthcoords, region=None, r3d=None):
    """Short handle to call view3d_utils.region_2d_to_location_3d in a more practical manner."""

    if region is None :
        region = bpy.context.region
    if r3d is None :
        r3d = bpy.context.space_data.region_3d

    return view3d_utils.region_2d_to_location_3d(region, r3d, viewcoords, depthcoords)


def get_viewport_width_height():
    'Returns the current height and width of the viewport'
    for a in bpy.context.screen.areas:
        if a.type == 'VIEW_3D':
            for r in a.regions:
                if r.type == 'WINDOW':
                    return r.width, r.height


def get_position_min_max(vectors) :
    """Returns min and max values for X and Y coordinates for a list of points.
    
    E.g : min_x, min_y, max_x, max_y = Deformer.get_position_min_max(self.get_geo())"""

    positions = []
    for p in vectors : 
        # Column vector of the position
        v = np.array(p)[:,np.newaxis].T
        # print(v)
        positions.append(v)
    
    # Matrix of X and Y positions
    position_matrix = np.concatenate(positions)[:,0:2].T

    # Minima and maxima calculations
    all_x, all_y = position_matrix[:, 0], position_matrix[:, 1]
    min_x, min_y = np.min(all_x), np.min(all_y)
    max_x, max_y = np.max(all_x), np.max(all_y)

    return min_x, min_y, max_x, max_y


def get_2D_bounding_box(geo) :
    """Return the width and height of the selected grease pencil strokes.
    Useful to calculate the size of deformation boxes and such.

    e.g. : x_worldsize, y_worldsize, center, centroid = get_2D_bounding_box(self.get_geo())
    
    Params : 
    - geo : Array of location points. Usually obtained through get_geo
    
    Returns : 
    - x_worldsize : size of the cluster on the x direction
    - y_worldsize : size of the cluster on the y direction
    - center : 2D local coordinates of the center of the cluster
    - centroid : 3D cendtroid of the cluster (mean of values)"""

    local_points = [location_to_region(p) for p in geo]

    centroid = np.mean(geo, axis=0)

    # Minima and maxima in 2D space
    all_x, all_y = np.array(local_points)[:, 0], np.array(local_points)[:, 1]
    min_x, min_y = np.min(all_x), np.min(all_y)
    max_x, max_y = np.max(all_x), np.max(all_y)

    width = (max_x - min_x)
    height = (max_y - min_y)
    center_x = min_x + (width/2)
    center_y = min_y + (height/2)

    # Centroid calculation
    centroid2d = (center_x,center_y)
    center = region_to_location(centroid2d, centroid)

    # X worldsize calculation
    x0 = region_to_location((min_x, min_y), centroid)
    x1 = region_to_location((max_x, min_y), centroid)
    x_worldsize = (x0 - x1).length

    # Y worldsize calculation
    y0 = region_to_location((min_x, min_y), centroid)
    y1 = region_to_location((min_x, max_y), centroid)
    y_worldsize = (y0 - y1).length

    return x_worldsize, y_worldsize, center, centroid


def get_3D_center_of_mass(geo) :
    """Get the mean value of every point in the cluster.
    - geo : list of 3D mathutils.Vectors"""

    geo = np.array(geo)
    centroid = np.mean(geo, axis=0)
    return centroid


def to_world(points, world_matrix) :
    """Applies the world_matrix to a set of local points. Not thoroughly tested, 
    use with caution, kept it here for potential future use. """
    
    # Decompose world_matrix into a rotation+scale matrix and a location vector
    world_mat = np.asarray(world_matrix)
    mat = world_mat[:3, :3]
    loc = world_mat[:3, 3]

    # Apply rotation matrix, then translate by location
    global_points = points @ mat.T + loc

    return global_points


def to_local(points, world_matrix) :
    """Applies world matrix backwards to world points to get them back to local space.
    
    - points : np.array of shape (N,3), N being the number of points
    - world_matrix : mathutils.Matrix outputted by obj.data.matrix_world
    
    Returns : np.array of shape (N,3) of local points"""

    # Decompose world_matrix into a rotation+scale matrix and a location vector
    world_mat = np.asarray(world_matrix)
    mat = world_mat[:3, :3]
    loc = world_mat[:3, 3]

    # Calculate actual inverse matrix instead of transpose in case 
    # of there being shear and mat not being a rotation matrix stricto sensu
    inv_mat = np.linalg.inv(mat.T)

    # Translate back, then apply inverse matrix
    local_points = (points - loc) @ inv_mat

    return local_points

def get_region_settings() :
        """Returns region settings for storage. Useful for 
        geometry_3D.region_to_location for example.
        
        Returns : 
        - r3d : 3D region
        - region : region"""

        for a in bpy.context.screen.areas:
            if a.type == 'VIEW_3D':
                space = a.spaces.active
                r3d = space.region_3d
                region = a.regions[-1]
                return r3d, region


def oriented_2D_angle(u,v) :
    """Calculates oriented angle of a 2D vector
    
    - u,v : 2D vectors in np.array format
    
    Returns : angle in radians, with sign for clockwise/anticlockwise rotation"""

    angle = np.arccos(np.dot(u,v)/(np.linalg.norm(u)*np.linalg.norm(v)))
    
    det = u[0]*v[1] - u[1]*v[0]
    if det < 0 :
        angle = -angle
    
    return angle

def middle_point(p1, p2) :
    """Returns the middle point between to points. Simple as that."""
    d = p2 - p1
    m = p1+d/2
    return m


def rmat(angle, plane) :
    assert(plane in ["X", "Y", "Z"])

    cosalpha = np.cos(angle)
    sinalpha = np.sin(angle)

    rot = None
    
    if plane == "Z" :
        rot = np.array([
            [cosalpha, -sinalpha,  0],
            [sinalpha,  cosalpha,  0],
            [       0,         0,  1]
        ])
    elif plane == "X" :
        rot = np.array([
            [1,        0,         0],
            [0, cosalpha, -sinalpha], 
            [0, sinalpha,  cosalpha],
        ])
    elif plane == "Y" :
        rot = np.array([
            [ cosalpha, 0, sinalpha],
            [        0, 1,        0],
            [-sinalpha, 0, cosalpha],
        ])
    
    return rot

def rotate(v, angle, plane="Z") :
    """Rotates angle along a plane.
    
    - v : vector as np.array
    - angle : float oriented angle in radians
    - plane : 'X', 'Y', or 'Z'
    
    Returns rotated vector as np.array"""

    assert(plane in ["X", "Y", "Z"])

    rot = rmat(angle, plane)

    return rot @ v

def calculate_intersection(p0, n, l0, l) :
    """Calculates the intersection between a line and a plane.
    
    - p0 : any point of the plane (np.array)
    - n : normal vector of the plane (np.array)
    - l0 : any point of the line (np.array)
    - l : direction vector of the line (np.array)
    
    Returns intersection as a 3D np.array"""

    d = np.dot(p0-l0, n)/np.dot(l, n)
    p = l0 + l*d
    return p
