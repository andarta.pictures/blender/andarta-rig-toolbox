# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "Andarta Rig Toolbox",
    "author" : "Tom VIGUIER",
    "description" : "",
    "blender" : (3, 6, 0),
    "version" : (0, 1, 2),
    "location" : "",
    "warning" : "",
    "category" : "Andarta"
}

import bpy
from .operators import *
#from .sanity_check.sanity_check import *
#from .sanity_check.sanity_ops import *

#from .sanity_check.sanity_ui import *
from .y_fight_check import *

class RIGTOOLS_PT_PANEL(bpy.types.Panel):
   
    bl_label = "Rig Tools"
    bl_idname = "RIGTOOLS_PT_PANEL"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Andarta"

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        row.prop(context.scene.tool_settings, 'use_gpencil_draw_additive',text = 'additive drawing', icon = 'FREEZE', emboss = context.scene.tool_settings.use_gpencil_draw_additive )

        row = layout.row()
        ops = row.operator('rigtools.obj_guides', text = 'add guides', icon = 'ONIONSKIN_ON', emboss=True)
        ops.add = True
        ops = row.operator('rigtools.obj_guides', text = 'remove guides', icon = 'ONIONSKIN_ON', emboss=True)
        ops.add = False

        row = layout.row()
        row.operator('rigtools.add_hide_helpers_mod', text = 'add hide helper modifiers', icon = 'MODIFIER', emboss=True)
        row.operator('rigtools.add_stroke_thick_mod', text = 'add stroke thickness modifiers', icon = 'MODIFIER', emboss=True)
        row = layout.row()
        row.operator('rigtools.cloneflip_member', text = 'clone_member', icon = 'ONIONSKIN_ON', emboss=True)
        # row.operator('rigtools.old_flip_animation', text = 'old flip animation', icon = 'ONIONSKIN_ON', emboss=True)
        row.operator('rigtools.flip_animation', text = 'flip animation', icon = 'ONIONSKIN_ON', emboss=True)
        row = layout.row()
        row.operator('rigtools.remove_holds', text = 'Remove holds', icon = 'NEXT_KEYFRAME', emboss=True)
        row.operator('rigtools.force_interpo', text = 'Force interpo', icon = 'PARTICLE_POINT', emboss=True)
        row.operator('rigtools.gp_place_gp_origin', text = 'replace origin', icon = 'TRANSFORM_ORIGINS', emboss=True)
        row = layout.row()
        row.operator('rigtools.switch_strokes', text = 'Switch strokes indices', icon = 'MOD_MIRROR', emboss=True)
        row.operator('stroke.hardness', text = 'edit stroke', icon = 'EVENT_H', emboss=True)
        row = layout.row()
        row.operator('rigtools.update_strokes', text = 'update strokes', icon = 'OUTLINER_DATA_GREASEPENCIL', emboss=True)
        row.operator('rigtools.del_small_strokes', text = 'Delete small strokes', icon = 'GP_SELECT_POINTS', emboss=True)
        ops = row.operator('rigtools.y_fight', text = 'Y Fight Check', icon = 'EVENT_Y', emboss=True, depress=context.scene.yfight_on)
        row = layout.row()
        ops = row.operator('rigtools.unlock_all', text = 'Unlock all (safe)', icon = 'UNLOCKED', emboss=True)
        ops.safe_lock = True
        ops = row.operator('rigtools.unlock_all', text = 'Unlock all', icon = 'UNLOCKED', emboss=True)
        ops.safe_lock = False
        row = layout.row()
        row.operator('rigtools.copy_obj_transforms', text = 'Copy transforms', icon ='COPYDOWN')
        if len(context.scene.clipboard.transforms) > 0 :
            row.operator('rigtools.paste_obj_transforms', text = 'Paste', icon = 'PASTEDOWN')
        

cls = [RIGTOOLS_PT_PANEL,
       RT_OT_ADD_HIDE_HELPERS_MOD,
       RT_OT_CLONEFLIP_MEMBER,
       RT_OT_FLIP_ANIMATION,
       RT_OT_OLD_FLIP_ANIMATION,
       RT_OT_UNLOCK_ALL,
       RT_OT_OBJGUIDES,
       RT_OT_MIRROR_GPFRAME,
       RT_OT_FORCE_INTERPO,
       RT_OT_SWITCH_STROKES,
       RT_OT_DEL_SMALL_STROKES,
       RT_OT_ADD_STROKE_THICK_MOD,
       RT_OT_REMOVE_HOLDS,
       RT_OT_Y_FIGHT,
       hardnessOperator,
       RIGTOOLS_OT_place_gp_origin,
       RT_OT_UPDATE_STROKES,
       RT_CLIPBOARD,
       RT_OT_COPY_OBJ_TRANSFORMS,
       RT_OT_PASTE_OBJ_TRANSFORMS
    ]
def register():
    for cl in cls :
        bpy.utils.register_class(cl)
    bpy.types.Scene.clipboard = bpy.props.PointerProperty(type=RT_CLIPBOARD)
    bpy.types.Scene.yfight_on = bpy.props.BoolProperty()
    bpy.types.Scene.yfight_precision = bpy.props.IntProperty()
    bpy.app.handlers.depsgraph_update_post.append(yfight_handler)
    bpy.app.handlers.frame_change_post.append(yfight_handler)

def unregister():
    for cl in cls :
        bpy.utils.unregister_class(cl)

    del bpy.types.Scene.clipboard
    del bpy.types.Scene.yfight_on
    del bpy.types.Scene.yfight_precision
    if yfight_handler in bpy.app.handlers.depsgraph_update_post:
        bpy.app.handlers.depsgraph_update_post.remove(yfight_handler)
    if yfight_handler in bpy.app.handlers.frame_change_post:
        bpy.app.handlers.frame_change_post.remove(yfight_handler)

